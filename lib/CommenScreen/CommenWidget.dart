// ignore_for_file: file_names

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget text(String text,
    {required Color color,
    required FontWeight weight,
    required double size,
    double height = 1,
    int? maxLines,
    bool? softWrap,
    TextDecoration? textDecoration,
    TextAlign? align}) {
  return Text(
    text,
    maxLines: maxLines,
    textAlign: align,
    softWrap: softWrap,
    style: GoogleFonts.notoSans(
      color: color,
      fontWeight: weight,
      fontSize: size,
      height: height,
      decoration: textDecoration,
      decorationColor: color,
    ),
  );
}
