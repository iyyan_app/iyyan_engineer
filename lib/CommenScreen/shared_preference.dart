import 'package:shared_preferences/shared_preferences.dart';

class AuthServices {
  static const String _authKey = 'authkey';
  static const String _userId = 'id';

  static Future<void> saveAuthkey(String authKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(_authKey, authKey);
  }

  static Future<String?> getAuthKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_authKey);
  }

  static Future<void> saveUserid(String userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(_userId, userId);
  }

  static Future<String?> getUserid() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_userId);
  }
}
