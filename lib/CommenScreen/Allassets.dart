import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';

var bname = SvgPicture.asset(
  'assets/bname.svg',
);
var bcompany = SvgPicture.asset(
  'assets/bcompany.svg',
);
var blocation = SvgPicture.asset(
  'assets/blocation.svg',
);
var bmobile = SvgPicture.asset(
  'assets/bmobile.svg',
);
var bexperience = SvgPicture.asset(
  'assets/bexperience.svg',
);
var customercare = SvgPicture.asset(
  'assets/customercare.svg',
);
var custcare = Image.asset(
  'assets/custcare.png',
  fit: BoxFit.fill,
);
var orangepassword = Image.asset(
  'assets/changepassorange.svg',
);
var bspecialist = SvgPicture.asset(
  'assets/bspecialist.svg',
);
var bavailability = SvgPicture.asset(
  'assets/bavailability.svg',
);
var bcosthour = SvgPicture.asset(
  'assets/bcosthour.svg',
);

var bell = SvgPicture.asset(
  'assets/bell.svg',
  fit: BoxFit.contain,
);

var backgroundImage = SvgPicture.asset(
  'assets/fullimg.svg',
  fit: BoxFit.contain,
);
var Email = SvgPicture.asset(
  'assets/email.svg',
);
var Lock = SvgPicture.asset(
  'assets/lock.svg',
);
var callDash = SvgPicture.asset(
  'assets/call_dash.svg',
);
var manImg = SvgPicture.asset(
  'assets/man_img.svg',
);
var Nameimg = SvgPicture.asset(
  'assets/nameimg.svg',
);
var favheart = SvgPicture.asset(
  'assets/favheart.svg',
);
var wallet = SvgPicture.asset(
  'assets/wallet.svg',
);
var logout = SvgPicture.asset(
  'assets/logout.svg',
);
var logout2 = SvgPicture.asset(
  'assets/logout.svg',
  color: AppColors.overalltextcolor,
);
var changepass = SvgPicture.asset(
  'assets/changepass.svg',
);
var changepass2 = SvgPicture.asset(
  'assets/changepass.svg',
  color: AppColors.chekboxActiveColor,
);
var mobile = SvgPicture.asset(
  'assets/mobile.svg',
);
var location = SvgPicture.asset(
  'assets/location.svg',
);
var experience = SvgPicture.asset(
  'assets/experience.svg',
);
var specialist = SvgPicture.asset(
  'assets/specialist.svg',
);
var availability = SvgPicture.asset(
  'assets/availability.svg',
);
var costperhour = SvgPicture.asset(
  'assets/costperhour.svg',
);
var oval1 = SvgPicture.asset(
  'assets/ovalshade1.svg',
);
var oval2 = SvgPicture.asset(
  'assets/ovalshade2.svg',
);
var loginarrow = SvgPicture.asset(
  'assets/loginarrow.svg',
  width: 10,
  height: 15,
);
var detailimg = SvgPicture.asset(
  'assets/detailimg.svg',
);
var iyyanlogo = Image.asset(
  'assets/iyyanlogo2.png',
);
var service = SvgPicture.asset(
  'assets/service.svg',
);
var botlocation = SvgPicture.asset(
  'assets/bottomlocation.svg',
);
var chatBlack = SvgPicture.asset(
  'assets/chatblack.svg',
);
var locRatio = SvgPicture.asset(
  'assets/locationRadio.svg',
);
var clockIcon = SvgPicture.asset(
  'assets/clockicon.svg',
);
var evviLogo = SvgPicture.asset(
  'assets/evvilogo.svg',
);
var evviNew = Image.asset(
  'assets/evvinew.png',
);
var postponed = SvgPicture.asset(
  'assets/postponed.svg',
);
var paySplash = SvgPicture.asset(
  'assets/paysplash.svg',
);
var attachment = SvgPicture.asset(
  'assets/attachment.svg',
);
var chatSend = SvgPicture.asset(
  'assets/chatsend.svg',
);
var attachmentBlack = SvgPicture.asset(
  'assets/attachment.svg',
  color: AppColors.commonblack,
);
var playButton = SvgPicture.asset(
  'assets/playButton.svg',
);
var qrImage = SvgPicture.asset(
  'assets/qrimg.svg',
);
var snadclock = SvgPicture.asset(
  'assets/sandclockimg.svg',
  fit: BoxFit.cover,
);
var pauseButton = SvgPicture.asset(
  'assets/pauseButton.svg',
);
var recentIcon = SvgPicture.asset(
  'assets/resent.svg',
);
var locationTop = SvgPicture.asset(
  'assets/locationtop.svg',
);
var location1 = SvgPicture.asset(
  'assets/location.svg',
);
var phone = SvgPicture.asset(
  'assets/phone.svg',
);
var experiance = SvgPicture.asset(
  'assets/experiance.svg',
);
var botCall = SvgPicture.asset(
  'assets/bottomcall.svg',
);
var speciallist = SvgPicture.asset(
  'assets/speciallist.svg',
);
var available = SvgPicture.asset(
  'assets/available.svg',
);
var cost = SvgPicture.asset(
  'assets/cost.svg',
);
var Right = SvgPicture.asset(
  'assets/right.svg',
);
var cont = SvgPicture.asset(
  'assets/cont.svg',
);
var ret = SvgPicture.asset(
  'assets/return.svg',
);
var banner = SvgPicture.asset(
  'assets/eng_banner.svg',
);

var LocationSearch = SvgPicture.asset(
  'assets/locationsearch.svg',
  width: 60,
  height: 60,
);
var notification = SvgPicture.asset(
  'assets/notification.svg',
  width: 35,
  height: 35,
);
var activehome = SvgPicture.asset(
  'assets/activehome.svg',
  width: 25,
  height: 25,
);
var inactivehome = SvgPicture.asset(
  'assets/inactivehome.svg',
  width: 25,
  height: 25,
);
var activeprofile = SvgPicture.asset(
  'assets/activeprofile.svg',
  width: 25,
  height: 25,
);
var inactiveprofile = SvgPicture.asset(
  'assets/inactiveprofile.svg',
  width: 25,
  height: 25,
);
var activemic = SvgPicture.asset(
  'assets/activemic.svg',
  width: 25,
  height: 25,
);
var inactivemic = SvgPicture.asset(
  'assets/inactivemic.svg',
  width: 25,
  height: 25,
);
var techsupport = SvgPicture.asset(
  'assets/techsup.svg',
);
var activehistory = SvgPicture.asset(
  'assets/activehistory.svg',
);
var activecomplete = SvgPicture.asset(
  'assets/activecomplete.svg',
);
var activepending = SvgPicture.asset(
  'assets/activepending.svg',
);
var righttick = SvgPicture.asset(
  'assets/righttick.svg',
  width: 20,
  height: 20,
);
var wrong = SvgPicture.asset(
  'assets/wrong.svg',
  width: 20,
  height: 20,
);
var overallser = SvgPicture.asset(
  'assets/overallser.svg',
);
var monthly = SvgPicture.asset(
  'assets/monthly.svg',
);
var dp = SvgPicture.asset(
  'assets/dp.svg',
);
