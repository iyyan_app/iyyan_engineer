import 'dart:math';
import 'package:flutter/cupertino.dart';

class Sizes {
  static double h = 0, w = 0, crosslength = 0;
  static double px8 = 0,
      px10 = 0,
      px12 = 0,
      px14 = 0,
      px16 = 0,
      px18 = 0,
      px20 = 0,
      px25 = 0;

  static void init(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    crosslength = pow((h * h) + (w * w), 1 / 2) as double;

    px8 = Sizes.crosslength * 0.009; //fixed
    px10 = Sizes.crosslength * 0.012; //fixed
    px12 = Sizes.crosslength * 0.014; //fixed
    px14 = Sizes.crosslength * 0.0155; //fixed
    px16 = Sizes.crosslength * 0.018; //fixed
    px18 = Sizes.crosslength * 0.020; //fixed
    px20 = Sizes.crosslength * 0.022; //fixed
    px25 = Sizes.crosslength * 0.03; //fixed
  }
}
