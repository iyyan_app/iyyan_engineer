import 'package:flutter/material.dart';

class AppColors {
  static const Color background_Color = Color(0XFFFFFFFF);
  static const Color loginAndsigin = Color.fromRGBO(50, 50, 50, 0.7);
  static const Color Label = Color(0XFF72A1FC);
  static const Color buttontextColor = Color(0XFFFFFFFF);
  static const Color buttoncolor = Color(0XFF131966);
  static const Color signuptext = Color(0XFF1C4FB3);
  static const Color chekboxActiveColor = Color(0XFFF18929);
  static const Color membershipinactiveColor = Color(0xfffe1e3ff);
  static const Color demoCardcolor = Color(0xfffa2a7e6);
  static const Color demoshadowcolor = Color(0xfff6d6d6d);
  static const Color commenTextColor = Color(0xfff252525);
  static const Color primiumCardcolor = Color(0xfff4650c8);
  static const Color LocationbarColor = Color(0xFFE7F1FA);
  static const Color badgeColor = Color(0xfff1b721e);
  static const Color ActiveTabcolor = Color(0xfff5fac65);
  static const Color upcommingcolor = Color(0xfff323232);
  static const Color commentlablecolor = Color(0xfff6e7e73);
  static const Color acceptbuttoncolor = Color(0xfff57c54d);
  static const Color declinecolor = Color(0xfffeb4343);
  static const Color overallcolor = Color(0xffffe2e5);
  static const Color overallhourcolor = Color(0xffffff4de);
  static const Color overallrating = Color(0xffff3e8ff);
  static const Color overalltextcolor = Color(0xFF425166);
  static const Color montlyincome = Color(0xFFdcfce7);
  static const Color completegreen = Color(0XFF5D9A1F);
  static const Color completegreen2 = Color(0XFFDCFCE7);
  static const Color servicepage = Color(0XFF000529);
  static const Color commonblack = Color(0XFF000000);
  static const Color lightblack = Color(0XFF444444);
  static const Color pureblue = Color(0XFF110F24);
  static const Color cancelBlack = Color(0XFF232323);
  static const Color yourLocation = Color(0XFF5479FB);
  static const Color notifysub = Color(0XFF6C6C6C);
  static const Color yourFeedback = Color(0XFFCCCCCC);
  static const Color starColor = Color(0XFFFBCC38);

  static const LinearGradient linear1 = LinearGradient(
      begin: Alignment.bottomRight,
      end: Alignment.topLeft,
      colors: [
        Color.fromRGBO(19, 25, 102, 1),
        Color.fromRGBO(19, 25, 102, 0.5),
        Color.fromRGBO(19, 25, 102, 1)
      ]);
}
