import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iyyan/App/OnSelectList/analysis2_scr.dart';
import 'package:iyyan/App/OnSelectList/analysis3_scr.dart';
import 'package:iyyan/App/OnSelectList/analysis_report.dart';
import 'package:iyyan/App/OnSelectList/analysis_scr.dart';
import 'package:iyyan/App/OnSelectList/cancelation_scr.dart';
import 'package:iyyan/App/OnSelectList/chat_scr.dart';
import 'package:iyyan/App/OnSelectList/firstotp_scr.dart';
import 'package:iyyan/App/OnSelectList/fourthotp_scr.dart';
import 'package:iyyan/App/OnSelectList/new_dash.dart';
import 'package:iyyan/App/OnSelectList/postponed_scr.dart';
import 'package:iyyan/App/OnSelectList/qr_screen.dart';
import 'package:iyyan/App/OnSelectList/quatation_scr.dart';
import 'package:iyyan/App/OnSelectList/secontotp_scr.dart';
import 'package:iyyan/App/OnSelectList/splash_payment.dart';
import 'package:iyyan/App/OnSelectList/submited_report.dart';
import 'package:iyyan/App/OnSelectList/thirdotp_scr.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

import 'OnSelectList/map_screen.dart';

String apiKey = 'AIzaSyD0_9uncWVbZ5MO9S9hr7sRks_IB5Q6GEw';

class DashboardMenu extends StatefulWidget {
  final Function(int) onSelectScreen;

  const DashboardMenu({super.key, required this.onSelectScreen});

  @override
  State<DashboardMenu> createState() => _DashboardMenuState();
}

int selectedScreenIndex = 0;

class _DashboardMenuState extends State<DashboardMenu> {
  @override
  Widget build(BuildContext context) {
    return IndexedStack(index: selectedScreenIndex, children: [
      MenuDash(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      Cancelation(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      MapScreen(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      FirstOtp(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      AnalysisScr(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      AnalysisReport(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      SecontOtp(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      Analysis2Scr(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      ThirdOtp(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      FourthOtp(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      Analysis3Scr(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      PostponedScr(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      SubmittedScr(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      QuatationScr(
        onSelectedScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      PaymentSplash(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      ChatScr(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
      QRScreen(
        onSelectScreen: (index) {
          setState(() {
            selectedScreenIndex = index;
          });
        },
      ),
    ]);
  }
}

class History1 extends StatefulWidget {
  final Function(int) onSelectPage1;
  const History1({super.key, required this.onSelectPage1});

  @override
  State<History1> createState() => _History1State();
}

class _History1State extends State<History1> {
  List<dynamic> historyrating = [
    {
      "Id": 1,
      "Name": 'Overall Services',
      "image": 'assets/overallser.svg',
      "value": '165'
    },
    {
      "Id": 2,
      "Name": 'Total hours',
      "image": 'assets/overallhour.svg',
      "value": '2hr'
    },
    {
      "Id": 3,
      "Name": 'Overall rating',
      "image": 'assets/overallrate.svg',
      "value": '4.6'
    }
  ];
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 57.h,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 5.h,
              width: double.infinity,
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(left: 25),
              child: text('Performance history',
                  color: AppColors.upcommingcolor,
                  weight: FontWeight.w600,
                  size: 12.sp),
            ),
            Container(
              height: 52.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 52.h,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: const BoxDecoration(
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8))),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 15.h,
                        width: double.infinity,
                        child: Row(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: List.generate(
                                  historyrating.length,
                                  (index) => InkWell(
                                        onTap: () {
                                          if (index == 0) {
                                            print('index 0');
                                          } else if (index == 1) {
                                            print('index 1');
                                          } else if (index == 2) {
                                            print('index2');
                                          }
                                        },
                                        child: Container(
                                          height: double.infinity,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.3,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Container(
                                                height: 10.h,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.23,
                                                decoration: BoxDecoration(
                                                    boxShadow: const [
                                                      BoxShadow(
                                                        color: Color.fromRGBO(
                                                            115,
                                                            115,
                                                            115,
                                                            0.25),
                                                        offset: Offset(1, 3),
                                                        blurRadius: 5,
                                                        spreadRadius: 1,
                                                      )
                                                    ],
                                                    color: historyrating[index]
                                                                ['Id'] ==
                                                            1
                                                        ? AppColors.overallcolor
                                                        : historyrating[index]
                                                                    ['Id'] ==
                                                                2
                                                            ? AppColors
                                                                .overallhourcolor
                                                            : AppColors
                                                                .overallrating,
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                alignment: Alignment.center,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    SvgPicture.asset(
                                                      '${historyrating[index]['image']}',
                                                      width: 20,
                                                      height: 20,
                                                    ),
                                                    text(
                                                        '${historyrating[index]['value']}',
                                                        color: AppColors
                                                            .overalltextcolor,
                                                        weight: FontWeight.w600,
                                                        size: 15.sp)
                                                  ],
                                                ),
                                              ),
                                              text(
                                                  '${historyrating[index]['Name']}',
                                                  color:
                                                      AppColors.upcommingcolor,
                                                  weight: FontWeight.w500,
                                                  size: 8.sp)
                                            ],
                                          ),
                                        ),
                                      )),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 15.h,
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: Container(
                          height: double.infinity,
                          width: MediaQuery.of(context).size.width * 0.6,
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: 10.h,
                                width: MediaQuery.of(context).size.width * 0.5,
                                decoration: const BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color:
                                            Color.fromRGBO(115, 115, 115, 0.25),
                                        offset: Offset(1, 3),
                                        blurRadius: 5,
                                        spreadRadius: 1,
                                      )
                                    ],
                                    color: AppColors.montlyincome,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    monthly,
                                    SizedBox(
                                      width: 5.w,
                                    ),
                                    text('\u20B9 25,460',
                                        color: AppColors.overalltextcolor,
                                        weight: FontWeight.w600,
                                        size: 15.sp)
                                  ],
                                ),
                              ),
                              text('Monthly Income',
                                  color: AppColors.upcommingcolor,
                                  weight: FontWeight.w500,
                                  size: 8.sp)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 23.h,
                        child: Column(
                          children: [
                            Container(
                              height: 5.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.only(left: 10),
                              child: text('Comments',
                                  color: AppColors.upcommingcolor,
                                  weight: FontWeight.w600,
                                  size: 10.sp),
                            ),
                            Container(
                              height: 20.h,
                              padding: const EdgeInsets.all(5),
                              child: ListView.builder(
                                  itemCount: 1,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: [
                                        Container(
                                          margin:
                                              const EdgeInsets.only(bottom: 10),
                                          height: 12.h,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          decoration: const BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color.fromRGBO(
                                                      115, 115, 115, 0.25),
                                                  offset: Offset(1, 3),
                                                  blurRadius: 5,
                                                  spreadRadius: 1,
                                                )
                                              ],
                                              color: AppColors.LocationbarColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          child: Center(
                                            child: Column(
                                              children: [
                                                ListTile(
                                                  leading: const SizedBox(
                                                      height: 50,
                                                      width: 50,
                                                      child: CircleAvatar()),
                                                  title: text('Raj',
                                                      color: AppColors
                                                          .upcommingcolor,
                                                      weight: FontWeight.w400,
                                                      size: 10.sp),
                                                  subtitle: text(
                                                      '14-04-2023 02:25 PM',
                                                      color:
                                                          const Color.fromRGBO(
                                                              0, 0, 0, 0.4),
                                                      weight: FontWeight.w400,
                                                      size: 10.sp),
                                                  trailing: const Icon(
                                                    Icons.star,
                                                    color: Colors.green,
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {},
                                                  child: text(
                                                      '“ Super service and Good response “',
                                                      color: AppColors
                                                          .upcommingcolor,
                                                      weight: FontWeight.w400,
                                                      size: 10),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Complete1 extends StatefulWidget {
  final Function(int) onSelectPage1;

  const Complete1({super.key, required this.onSelectPage1});

  @override
  State<Complete1> createState() => _Complete1State();
}

class _Complete1State extends State<Complete1> {
  List<dynamic> complete = [
    {
      'name': 'savari',
      'date/time': '02,04,2021 / 04.30',
      'issues': 'laptop screen',
      'service': 'onsite',
      'mobile': '9360788011',
      'cost': '1000',
      'addtcost': '200',
      'total': '1200',
      'isselected': false
    },
    {
      'name': 'savari',
      'date/time': '02,04,2021 / 04.30',
      'issues': 'laptop screen',
      'service': 'onsite',
      'mobile': '9360788011',
      'cost': '1000',
      'addtcost': '2000',
      'total': '1200',
      'isselected': false
    },
    {
      'name': 'savari',
      'date/time': '02,04,2021 / 04.30',
      'issues': 'laptop screen',
      'service': 'onsite',
      'mobile': '9360788011',
      'cost': '1000',
      'addtcost': '200',
      'total': '1200',
      'isselected': false
    },
  ];
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 57.h,
      child: Column(
        children: [
          Container(
            height: 5.h,
            width: double.infinity,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 25),
            child: text('Complete',
                color: AppColors.upcommingcolor,
                weight: FontWeight.w600,
                size: 12.sp),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.49,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
              itemCount: complete.length,
              itemBuilder: (BuildContext context, int index) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(115, 115, 115, 0.25),
                              offset: Offset(1, 3),
                              blurRadius: 3,
                              spreadRadius: 1,
                            )
                          ]),
                      child: Center(
                        child: Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.62,
                              height: 16.h,
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8))),
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          complete[index]['isselected'] =
                                              !complete[index]['isselected'];
                                        });
                                      },
                                      child: Icon(
                                        complete[index]['isselected']
                                            ? Icons.star
                                            : Icons.star_border,
                                        size: 20,
                                        color: complete[index]['isselected']
                                            ? Colors.red
                                            : Colors.black54,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, right: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.22,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Name',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.34,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['name']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, right: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.22,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Date/Time',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.34,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['date/time']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, right: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.22,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Service',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.34,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['service']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, right: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.22,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Issues',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.34,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['issues']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.32,
                              height: 16.h,
                              decoration: const BoxDecoration(
                                  color: AppColors.completegreen2,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      bottomRight: Radius.circular(8))),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.25,
                                      height: 3.h,
                                      decoration: BoxDecoration(
                                          color: AppColors.completegreen,
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                callDash,
                                                text(
                                                    '${complete[index]['mobile']}',
                                                    color: AppColors
                                                        .buttontextColor,
                                                    weight: FontWeight.w500,
                                                    size: 8.sp),
                                              ]),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.15,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Cost',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.12,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['cost']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.15,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Add Cost',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.12,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['addtcost']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 5, bottom: 5),
                                    child: Row(children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.15,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text('Total',
                                              color:
                                                  AppColors.commentlablecolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      ),
                                      const Text(
                                        '-',
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.12,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: text(
                                              '${complete[index]['total']}',
                                              color: AppColors.overalltextcolor,
                                              weight: FontWeight.w500,
                                              size: 10.sp),
                                        ),
                                      )
                                    ]),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class Pending1 extends StatefulWidget {
  final Function(int) onSelectPage1;
  final Function(int) onSelectScreen;

  const Pending1(
      {super.key, required this.onSelectPage1, required this.onSelectScreen});

  @override
  State<Pending1> createState() => _Pending1State();
}

class _Pending1State extends State<Pending1> {
  List<Map<String, dynamic>> issueImage = [
    {'title': 'savari'},
    {'title': 'savari'},
  ];
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 57.h,
      child: Column(
        children: [
          Container(
            height: 5.h,
            width: double.infinity,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 25),
            child: text('Pending',
                color: AppColors.upcommingcolor,
                weight: FontWeight.w600,
                size: 12.sp),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.49,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
                itemCount: 4,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        height: 11.h,
                        width: MediaQuery.of(context).size.width * 0.9,
                        decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(115, 115, 115, 0.25),
                                offset: Offset(1, 3),
                                blurRadius: 5,
                                spreadRadius: 1,
                              )
                            ],
                            color: AppColors.overallcolor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 7.h,
                              width: double.infinity,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.15,
                                    height: double.infinity,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        text('T-01',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp),
                                        text('#T01',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp)
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.28,
                                    height: double.infinity,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        text('Customer',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp),
                                        text('Virat',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp)
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.28,
                                    height: double.infinity,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        text('Issues',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp),
                                        text('Software Problem',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp)
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.19,
                                    height: double.infinity,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        text('Date',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp),
                                        text('13-1-2024',
                                            color: AppColors.commentlablecolor,
                                            weight: FontWeight.w500,
                                            size: 8.sp)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.center,
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width * 0.5,
                                height: double.infinity,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(15)),
                                      child: Material(
                                        child: InkResponse(
                                          onTap: () {
                                            bottomSheet();
                                          },
                                          child: Ink(
                                            height: 2.3.h,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.2,
                                            decoration: const BoxDecoration(
                                                color:
                                                    AppColors.acceptbuttoncolor,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(15))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                righttick,
                                                text('Accept',
                                                    color: AppColors
                                                        .background_Color,
                                                    weight: FontWeight.w400,
                                                    size: 8.sp)
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(15)),
                                      child: Material(
                                        child: InkWell(
                                          onTap: () {
                                            widget.onSelectScreen(1);
                                          },
                                          child: Ink(
                                            height: 2.3.h,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.2,
                                            decoration: const BoxDecoration(
                                                color: AppColors.declinecolor,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(15))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                wrong,
                                                text('Decline',
                                                    color: AppColors
                                                        .background_Color,
                                                    weight: FontWeight.w400,
                                                    size: 8.sp)
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }

  void bottomSheet() {
    showBottomSheet(
      context: context,
      builder: (context) => Container(
        height: 60.h,
        width: double.infinity,
        decoration: const BoxDecoration(
            color: AppColors.buttoncolor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        child: Column(
          children: [
            SizedBox(height: 0.5.h),
            GestureDetector(
              onTap: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                width: 10.h,
                height: 1.h,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        height: 2.5.h,
                        width: 2.5.h,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.acceptbuttoncolor),
                        child: const Icon(
                          Icons.check,
                          color: AppColors.buttoncolor,
                          size: 15,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      text('Accepting Ticket',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 8.sp)
                    ],
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        color: AppColors.background_Color,
                        borderRadius: BorderRadius.all(Radius.circular(3))),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: text('T01',
                          color: AppColors.buttoncolor,
                          weight: FontWeight.w500,
                          size: 6.sp),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      text('Narendran',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Name',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  ),
                  Column(
                    children: [
                      text('Screen Repair',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Issues',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      text('12.00am to 12.00pm',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Available Time',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  ),
                  Column(
                    children: [
                      text('Onsite',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Service Mode',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Material(
                      child: InkWell(
                        onTap: () {
                          widget.onSelectScreen(2);
                          Navigator.pop(context);
                        },
                        child: Ink(
                          height: 2.5.h,
                          width: 10.h,
                          decoration: BoxDecoration(
                              color: AppColors.chekboxActiveColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5, right: 5),
                            child: Row(
                              children: [
                                botlocation,
                                SizedBox(
                                  width: 1.h,
                                ),
                                text('Location',
                                    color: AppColors.background_Color,
                                    weight: FontWeight.w500,
                                    size: 8.sp),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Material(
                          child: InkWell(
                            onTap: () {
                              widget.onSelectScreen(15);
                              Navigator.pop(context);
                            },
                            child: Ink(
                              height: 2.5.h,
                              width: 3.h,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: AppColors.background_Color),
                              child: chatBlack,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Container(
                        height: 2.5.h,
                        width: 12.h,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: AppColors.acceptbuttoncolor),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 5, right: 5),
                          child: Row(
                            children: [
                              botCall,
                              SizedBox(
                                width: 1.w,
                              ),
                              text('9268708011',
                                  color: AppColors.background_Color,
                                  weight: FontWeight.w500,
                                  size: 8.sp),
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Column(
              children: [
                Container(
                  height: 12.h,
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      const BoxDecoration(color: AppColors.background_Color),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15, top: 5),
                            child: text('Issue description',
                                color: AppColors.lightblack,
                                weight: FontWeight.w500,
                                size: 10.sp),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: text(
                              'Screen Problem means an error condition that is repeatable ',
                              color: AppColors.lightblack,
                              weight: FontWeight.w500,
                              size: 10.sp),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 23.5.h,
              width: MediaQuery.of(context).size.width,
              decoration:
                  const BoxDecoration(color: AppColors.LocationbarColor),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 6,
                      mainAxisExtent: 150),
                  itemCount: issueImage.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey),
                      child: Center(child: Text(issueImage[index]['title'])),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
