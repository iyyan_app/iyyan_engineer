import 'package:flutter/material.dart';
import 'package:iyyan/App/profile_options/profile_common.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class Wallet extends StatefulWidget {
  final Function(int) onSelectPage;

  const Wallet({super.key, required this.onSelectPage});

  @override
  State<Wallet> createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  void toggleVisibility() {
    setState(() {
      isvisible = !isvisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.95,
        height: MediaQuery.of(context).size.height * 0.56,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(4),
            bottomRight: Radius.circular(4),
          ),
        ),
        child: Column(children: [
          ListTile(
            leading: wallet,
            title: text('Wallet',
                color: AppColors.commonblack,
                weight: FontWeight.w500,
                size: 14.sp),
            trailing: IconButton(
                onPressed: () {
                  setState(() {
                    toggleVisibility();
                    widget.onSelectPage(0);
                  });
                },
                icon: const Icon(Icons.arrow_back)),
          ),
        ]));
  }
}
