import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iyyan/App/profile_options/profile_common.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class Changepassword extends StatefulWidget {
  final Function(int) onSelectPage;

  const Changepassword({
    super.key,
    required this.onSelectPage,
  });

  @override
  State<Changepassword> createState() => _ChangepasswordState();
}

class _ChangepasswordState extends State<Changepassword>
    with SingleTickerProviderStateMixin {
  /* late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    );
  }*/
  TextEditingController oldpassController = TextEditingController();
  TextEditingController newpassController = TextEditingController();
  TextEditingController confirmpassController = TextEditingController();
  void toggleVisibility() {
    setState(() {
      isvisible = !isvisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.95,
      height: MediaQuery.of(context).size.height * 0.56,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(4),
          bottomRight: Radius.circular(4),
        ),
      ),
      child: Column(
        children: [
          ListTile(
            leading: changepass2,
            title: text('Change password',
                color: AppColors.commonblack,
                weight: FontWeight.w500,
                size: 14.sp),
            trailing: IconButton(
                onPressed: () {
                  setState(() {
                    toggleVisibility();
                    widget.onSelectPage(0);
                  });
                },
                icon: const Icon(Icons.arrow_back)),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.85,
            height: MediaQuery.of(context).size.height * 0.06,
            child: TextField(
              controller: oldpassController,
              decoration: InputDecoration(
                hintText: 'Old Password',
                hintStyle: GoogleFonts.notoSans(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: Colors.black87,
                ),
                labelText: 'Old Password',
                labelStyle: GoogleFonts.notoSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black87,
                ),
                border: const UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black54),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                ),
                focusedBorder: const UnderlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                  borderSide: BorderSide(color: Colors.black54),
                ),
                counterText: '',
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                //     errorText: valmobile ? 'mobile number is empty' : null,
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.85,
            height: MediaQuery.of(context).size.height * 0.06,
            child: TextField(
              controller: newpassController,
              decoration: InputDecoration(
                hintText: 'New Password',
                hintStyle: GoogleFonts.notoSans(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: Colors.black87,
                ),
                labelText: 'New Password',
                labelStyle: GoogleFonts.notoSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black87,
                ),
                border: const UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black54),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                ),
                focusedBorder: const UnderlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                  borderSide: BorderSide(color: Colors.black54),
                ),
                counterText: '',
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                //     errorText: valmobile ? 'mobile number is empty' : null,
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.85,
            height: MediaQuery.of(context).size.height * 0.06,
            child: TextField(
              controller: confirmpassController,
              decoration: InputDecoration(
                hintText: 'Confirm Password',
                hintStyle: GoogleFonts.notoSans(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: Colors.black87,
                ),
                labelText: 'Confirm Password',
                labelStyle: GoogleFonts.notoSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black87,
                ),
                border: const UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black54),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                ),
                focusedBorder: const UnderlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                  borderSide: BorderSide(color: Colors.black54),
                ),
                counterText: '',
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                //     errorText: valmobile ? 'mobile number is empty' : null,
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.15,
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.06,
            width: MediaQuery.of(context).size.width * 0.75,
            child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(AppColors.ActiveTabcolor),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
                child: Text(
                  'Set Password',
                  style: GoogleFonts.notoSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
