import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:iyyan/App/Profile.dart';
import 'package:iyyan/App/profile_options/logout.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class Profcom extends StatefulWidget {
  final Function(int) onSelectPage;

  const Profcom({super.key, required this.onSelectPage});

  @override
  State<Profcom> createState() => _ProfcomState();
}

bool isvisible = false;

class _ProfcomState extends State<Profcom> {
  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> profoptions = [
      {
        "id": 1,
        "name": 'Favourite',
        "svgImage": SvgPicture.asset('assets/favheart.svg'),
        'color': AppColors.LocationbarColor
      },
      {
        "id": 2,
        "name": 'Wallet',
        "svgImage": SvgPicture.asset('assets/wallet.svg'),
        'color': AppColors.LocationbarColor
      },
      {
        "id": 3,
        "name": 'Change Password',
        "svgImage": SvgPicture.asset('assets/changepass.svg'),
        'color': AppColors.ActiveTabcolor
      },
      {
        "id": 4,
        "name": 'Logout',
        "svgImage": SvgPicture.asset('assets/logout.svg'),
        'color': AppColors.chekboxActiveColor
      },
    ];
    void toggleVisibility() {
      setState(() {
        isvisible = !isvisible;
      });
    }

    final provider = Provider.of<ProviderScreen>(context, listen: false);
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.95,
          height: MediaQuery.of(context).size.height * 0.56,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(4),
              bottomRight: Radius.circular(4),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 30,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return const Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Icon(
                            Icons.star,
                            color: AppColors.chekboxActiveColor,
                            size: 15,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                text('Over all rating',
                    color: Colors.black, weight: FontWeight.w400, size: 10.sp),
                const SizedBox(
                  width: 5,
                ),
                text('4/6',
                    color: AppColors.chekboxActiveColor,
                    weight: FontWeight.w400,
                    size: 10.sp),
              ]),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.40,
                      height: MediaQuery.of(context).size.height * 0.07,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: AppColors.buttontextColor,
                        boxShadow: const [
                          BoxShadow(
                            color: Color.fromRGBO(115, 115, 115, 0.25),
                            offset: Offset(1, 3),
                            blurRadius: 5,
                            spreadRadius: 1,
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: location1,
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: Consumer<ProviderScreen>(
                                  builder: (context, mycity, child) {
                                return text(
                                  '${mycity.myCity}',
                                  color: AppColors.commentlablecolor,
                                  weight: FontWeight.w400,
                                  size: 10.sp,
                                  maxLines: 2,
                                );
                              }),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.40,
                    height: MediaQuery.of(context).size.height * 0.07,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.buttontextColor,
                      boxShadow: const [
                        BoxShadow(
                          color: Color.fromRGBO(115, 115, 115, 0.25),
                          offset: Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: mobile,
                        ),
                        Consumer<ProviderScreen>(
                            builder: (context, mynumber, child) {
                          return text('${mynumber.myNumber}',
                              color: AppColors.commentlablecolor,
                              weight: FontWeight.w400,
                              size: 10.sp);
                        })
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.40,
                    height: MediaQuery.of(context).size.height * 0.07,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.buttontextColor,
                      boxShadow: const [
                        BoxShadow(
                          color: Color.fromRGBO(115, 115, 115, 0.25),
                          offset: Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: experience,
                        ),
                        Consumer<ProviderScreen>(
                            builder: (context, myexperience, _) {
                          return text('${myexperience.myExperience}',
                              color: AppColors.commentlablecolor,
                              weight: FontWeight.w400,
                              size: 10.sp);
                        })
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.40,
                    height: MediaQuery.of(context).size.height * 0.07,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.buttontextColor,
                      boxShadow: const [
                        BoxShadow(
                          color: Color.fromRGBO(115, 115, 115, 0.25),
                          offset: Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: specialist,
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 5.0),
                            child: Consumer<ProviderScreen>(
                                builder: (context, mywork, _) {
                              return text('${mywork.myWork}',
                                  maxLines: 2,
                                  color: AppColors.commentlablecolor,
                                  weight: FontWeight.w400,
                                  size: 10.sp);
                            }),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.40,
                    height: MediaQuery.of(context).size.height * 0.07,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.buttontextColor,
                      boxShadow: const [
                        BoxShadow(
                          color: Color.fromRGBO(115, 115, 115, 0.25),
                          offset: Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: availability,
                        ),
                        Consumer<ProviderScreen>(
                            builder: (context, mytiming, _) {
                          return text('${mytiming.myTiming}',
                              color: AppColors.commentlablecolor,
                              weight: FontWeight.w400,
                              size: 10.sp);
                        })
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.40,
                    height: MediaQuery.of(context).size.height * 0.07,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.buttontextColor,
                      boxShadow: const [
                        BoxShadow(
                          color: Color.fromRGBO(115, 115, 115, 0.25),
                          offset: Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: costperhour,
                        ),
                        Consumer<ProviderScreen>(builder: (context, mycost, _) {
                          return text('${mycost.myCost}',
                              color: AppColors.commentlablecolor,
                              weight: FontWeight.w400,
                              size: 10.sp);
                        })
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.85,
                height: MediaQuery.of(context).size.height * 0.18,
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 15,
                      mainAxisExtent: 55),
                  itemCount: profoptions.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        height: MediaQuery.of(context).size.height * 0.07,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: profoptions[index]['color'],
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(115, 115, 115, 0.25),
                              offset: Offset(1, 3),
                              blurRadius: 5,
                              spreadRadius: 1,
                            )
                          ],
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              toggleVisibility();
                              widget.onSelectPage(index + 1);
                              if (index == 3) {
                                showDialog(
                                    context: context,
                                    builder: (context) => const Logout());
                                selectedPageIndex = 0;
                              }
                            },
                            child: Row(
                              children: [
                                Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: profoptions[index]['svgImage']),
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 5.0),
                                    child: text('${profoptions[index]['name']}',
                                        maxLines: 2,
                                        color: profoptions[index]['id'] == 3 ||
                                                profoptions[index]['id'] == 4
                                            ? Colors.white
                                            : AppColors.commentlablecolor,
                                        weight: FontWeight.w400,
                                        size: 10.sp),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ));
                  },
                ),
              ),
            ]),
          ),
        ),
      ],
    );
  }
}
