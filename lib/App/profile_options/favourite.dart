import 'package:flutter/material.dart';
import 'package:iyyan/App/profile_options/profile_common.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class Favourite extends StatefulWidget {
  final Function(int) onSelectPage;

  const Favourite({
    super.key,
    required this.onSelectPage,
  });

  @override
  State<Favourite> createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  final List<Map<String, dynamic>> favouritelist = [
    {
      'image': const AssetImage('assets/IMG.png'),
      'name': 'Naren',
      'city': 'Ambai',
      'isselected': false
    },
    {
      'image': const AssetImage('assets/IMG.png'),
      'name': 'Vicky',
      'city': 'Ambai',
      'isselected': false
    },
    {
      'image': const AssetImage('assets/IMG.png'),
      'name': 'Kannan',
      'city': 'Ambai',
      'isselected': false
    },
    {
      'image': const AssetImage('assets/IMG.png'),
      'name': 'Sathya',
      'city': 'Ambai',
      'isselected': false
    }
  ];
  void toggleVisibility() {
    setState(() {
      isvisible = !isvisible;
    });
  }

  bool favclick = false;
  int favselect = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.95,
      height: MediaQuery.of(context).size.height * 0.56,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(4),
          bottomRight: Radius.circular(4),
        ),
      ),
      child: Column(
        children: [
          ListTile(
            leading: favheart,
            title: text('Favourite cstomer',
                color: AppColors.commonblack,
                weight: FontWeight.w500,
                size: 14.sp),
            trailing: IconButton(
                onPressed: () {
                  setState(() {
                    toggleVisibility();
                    widget.onSelectPage(0);
                  });
                },
                icon: const Icon(Icons.arrow_back)),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.48,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: ListView.builder(
                itemCount: favouritelist.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                      leading: CircleAvatar(
                        backgroundImage: favouritelist[index]['image'],
                      ),
                      title: text('${favouritelist[index]['name']}',
                          color: AppColors.commonblack,
                          weight: FontWeight.w400,
                          size: 12.sp),
                      subtitle: text('${favouritelist[index]['city']}',
                          color: AppColors.lightblack,
                          weight: FontWeight.w400,
                          size: 10.sp),
                      trailing: IconButton(
                          onPressed: () {
                            setState(() {
                              favouritelist[index]['isselected'] =
                                  !favouritelist[index]['isselected'];
                            });
                          },
                          icon: Icon(
                            favouritelist[index]['isselected']
                                ? Icons.star
                                : Icons.star_border,
                            color: favouritelist[index]['isselected']
                                ? Colors.red
                                : Colors.black54,
                          )));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
