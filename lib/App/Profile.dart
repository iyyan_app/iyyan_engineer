import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iyyan/App/profile_options/changepassword.dart';
import 'package:iyyan/App/profile_options/favourite.dart';
import 'package:iyyan/App/profile_options/profile_common.dart';
import 'package:iyyan/App/profile_options/wallet.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:iyyan/CommenScreen/shared_preference.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import '../Auth/SignupDetails.dart';
import 'OnSelectList/Provider_scr.dart';

class Profile extends StatefulWidget {
  final VoidCallback onBack;

  const Profile({super.key, required this.onBack});

  @override
  State<Profile> createState() => _ProfileState();
}

bool containershow = false;
bool imageDecreased = false;
int selectedPageIndex = 0;

class _ProfileState extends State<Profile> {
  final nameController = TextEditingController();
  final servicecenterController = TextEditingController();
  final locationController = TextEditingController();
  final mobilenumController = TextEditingController();
  final experienceController = TextEditingController();
  final specializedController = TextEditingController();
  final availabilityController = TextEditingController();
  final costperhourController = TextEditingController();
  final costperhourController2 = TextEditingController();

  final namefocus = FocusNode();
  final costperhourFocus = FocusNode();
  final costperhourFocus2 = FocusNode();

  @override
  void dispose() {
    costperhourFocus.dispose();
    costperhourFocus2.dispose();
    super.dispose();
  }

  final ImagePicker _imagePicker = ImagePicker();
  File? imageFile2;

  String experienceYear = '1 - Years';
  var items = List.generate(6, (index) => '${index + 1} - Years');

  DateTime? fromDateTime;
  DateTime? toDateTime;

  Future<void> profilePut() async {
    try {
      String? key = await AuthServices.getAuthKey();
      String? id = await AuthServices.getUserid();
      if (id != null) {}
    } catch (e) {}
  }

  String getTimeText() {
    if (fromDateTime != null && toDateTime != null) {
      return '${(fromDateTime!.hour > 12) ? fromDateTime!.hour - 12 : fromDateTime!.hour} ${(fromDateTime!.hour >= 12) ? 'PM' : 'AM'} - ${(toDateTime!.hour > 12) ? toDateTime!.hour - 12 : toDateTime!.hour} ${(toDateTime!.hour >= 12) ? 'PM' : 'AM'}';
    } else {
      return 'Time';
    }
  }

  Future<void> _openCamera() async {
    final pickedImage =
        await _imagePicker.pickImage(source: ImageSource.camera);
    if (pickedImage != null) {
      setState(() {
        imageFile2 = File(pickedImage.path);
      });
    }
  }

  Future<void> _openGallery() async {
    final pickedImage =
        await _imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      setState(() {
        imageFile2 = File(pickedImage.path);
      });
    }
  }

  void timepick() {
    showDialog(
        context: context,
        builder: (context) {
          return Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.85,
              height: MediaQuery.of(context).size.height * 0.20,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_back_ios_new,
                            size: 20,
                          )),
                      const SizedBox(
                        width: 55,
                      ),
                      Text(
                        'Select Time',
                        style: GoogleFonts.notoSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.black,
                            decoration: TextDecoration.none),
                      ),
                    ],
                  ),
                  Container(
                    height: 1,
                    width: MediaQuery.of(context).size.width * 0.80,
                    color: Colors.black,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 55,
                        child: Row(
                          children: [
                            Text(
                              'From',
                              style: GoogleFonts.notoSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black,
                                  decoration: TextDecoration.none),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            TimePickerSpinner(
                              is24HourMode: false,
                              isShowSeconds: false,
                              normalTextStyle: const TextStyle(
                                  fontSize: 0,
                                  color: Colors.transparent,
                                  decoration: TextDecoration.none),
                              highlightedTextStyle: const TextStyle(
                                  fontSize: 20,
                                  color: Colors.black54,
                                  decoration: TextDecoration.none),
                              spacing: 5,
                              itemHeight: 25,
                              itemWidth: 25,
                              isForce2Digits: true,
                              onTimeChange: (time) {
                                setState(() {
                                  fromDateTime = time;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 55,
                        child: Row(
                          children: [
                            Text(
                              'To',
                              style: GoogleFonts.notoSans(
                                  fontSize: 1,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black,
                                  decoration: TextDecoration.none),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            TimePickerSpinner(
                              is24HourMode: false,
                              normalTextStyle: const TextStyle(
                                  fontSize: 0,
                                  color: Colors.transparent,
                                  decoration: TextDecoration.none),
                              highlightedTextStyle: const TextStyle(
                                  fontSize: 20,
                                  color: Colors.black54,
                                  decoration: TextDecoration.none),
                              spacing: 5,
                              itemHeight: 25,
                              itemWidth: 25,
                              isForce2Digits: true,
                              onTimeChange: (time) {
                                setState(() {
                                  toDateTime = time;
                                });
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 200),
                    child: SizedBox(
                      height: 25,
                      width: 90,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              AppColors.acceptbuttoncolor),
                          overlayColor: MaterialStateProperty.all(
                              AppColors.acceptbuttoncolor),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                        ),
                        child: Text(
                          'SAVE',
                          style: GoogleFonts.notoSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void showoption() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _openCamera();
                          Navigator.pop(context);
                        },
                        child: SizedBox(
                          height: 40,
                          width: 40,
                          child: Image.asset(
                            'assets/camera.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      const Text(
                        'Camera',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _openGallery();
                          Navigator.pop(context);
                        },
                        child: SizedBox(
                          height: 40,
                          width: 40,
                          child: Image.asset(
                            'assets/file.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      const Text(
                        'File',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderScreen>(context, listen: false);
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(gradient: AppColors.linear1),
        child: Stack(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [oval1, oval2],
          ),
          Positioned(
              top: 1,
              child: IconButton(
                  onPressed: widget.onBack,
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                    size: 22,
                  ))),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.10,
          ),
          AnimatedPositioned(
            top: containershow ? 40 : 69,
            bottom: containershow ? 0 : 0,
            left: 10,
            right: 10,
            duration: const Duration(milliseconds: 300),
            child: containershow
                ? Container(
                    width: MediaQuery.of(context).size.width * 0.95,
                    height: MediaQuery.of(context).size.height * 0.79,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6))),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        containershow = !containershow;
                                        imageDecreased = !imageDecreased;
                                        selectedPageIndex = 0;
                                      });
                                    },
                                    child: const Icon(
                                      Icons.arrow_back,
                                      size: 22,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () {
                                  showoption();
                                },
                                child: SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.40,
                                  height: 3.h,
                                  child: Center(
                                    child: text('Change Picture',
                                        color: AppColors.commentlablecolor,
                                        weight: FontWeight.w400,
                                        size: 14.sp),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.80,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  text('Name',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.80,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: AppColors.buttontextColor,
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Color.fromRGBO(
                                                115, 115, 115, 0.25),
                                            offset: Offset(1, 3),
                                            blurRadius: 5,
                                            spreadRadius: 1,
                                          )
                                        ],
                                      ),
                                      child: TextField(
                                        controller: nameController,
                                        maxLength: 20,
                                        textAlign: TextAlign.center,
                                        cursorColor: Colors.black54,
                                        cursorHeight: 25,
                                        cursorWidth: 1,
                                        decoration: InputDecoration(
                                          hintText: '${provider.myName}',
                                          counterText: '',
                                          prefixIcon: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: bname,
                                          ),
                                          suffix: const Padding(
                                            padding: EdgeInsets.only(right: 12),
                                            child: SizedBox(
                                              height: 18,
                                              width: 20,
                                            ),
                                          ),
                                          contentPadding:
                                              const EdgeInsets.all(8),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  text('Service center Name',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.80,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: AppColors.buttontextColor,
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Color.fromRGBO(
                                                115, 115, 115, 0.25),
                                            offset: Offset(1, 3),
                                            blurRadius: 5,
                                            spreadRadius: 1,
                                          )
                                        ],
                                      ),
                                      child: TextField(
                                        controller: servicecenterController,
                                        maxLength: 20,
                                        textAlign: TextAlign.center,
                                        cursorColor: Colors.black54,
                                        cursorHeight: 25,
                                        cursorWidth: 1,
                                        decoration: InputDecoration(
                                          counterText: '',
                                          hintText: '${provider.myCenter}',
                                          prefixIcon: Padding(
                                            padding: const EdgeInsets.all(10),
                                            child: SizedBox(
                                                height: 18,
                                                width: 20,
                                                child: bcompany),
                                          ),
                                          suffix: const Padding(
                                            padding: EdgeInsets.only(right: 12),
                                            child: SizedBox(
                                              height: 18,
                                              width: 20,
                                            ),
                                          ),
                                          contentPadding:
                                              const EdgeInsets.all(8),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  text('Location',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.80,
                                    height: MediaQuery.of(context).size.height *
                                        0.05,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppColors.buttontextColor,
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Color.fromRGBO(
                                              115, 115, 115, 0.25),
                                          offset: Offset(1, 3),
                                          blurRadius: 5,
                                          spreadRadius: 1,
                                        )
                                      ],
                                    ),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 17, right: 16),
                                            child: blocation,
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.55,
                                            child: Center(
                                              child: Text(
                                                provider.myCity!.isEmpty
                                                    ? 'Select your Location'
                                                    : provider.myCity!,
                                                style: GoogleFonts.notoSans(
                                                  fontSize: 10.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: const Color.fromRGBO(
                                                      34, 16, 0, 1),
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              onPressed: () {
                                                showDialog(
                                                    context: context,
                                                    builder: (context) =>
                                                        const LocationPick());
                                              },
                                              icon: const Icon(
                                                Icons.my_location_sharp,
                                                size: 22,
                                                color: Colors.black54,
                                              )),
                                        ]),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  text('Mobile Number',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.80,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: AppColors.buttontextColor,
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Color.fromRGBO(
                                                115, 115, 115, 0.25),
                                            offset: Offset(1, 3),
                                            blurRadius: 5,
                                            spreadRadius: 1,
                                          )
                                        ],
                                      ),
                                      child: TextField(
                                        controller: mobilenumController,
                                        maxLength: 10,
                                        keyboardType: TextInputType.number,
                                        textAlign: TextAlign.center,
                                        cursorColor: Colors.black54,
                                        cursorHeight: 25,
                                        cursorWidth: 1,
                                        decoration: InputDecoration(
                                          counterText: '',
                                          hintText: '${provider.myNumber}',
                                          prefixIcon: Padding(
                                            padding: const EdgeInsets.all(10),
                                            child: SizedBox(
                                                height: 18,
                                                width: 20,
                                                child: bmobile),
                                          ),
                                          suffix: const Padding(
                                            padding: EdgeInsets.only(right: 12),
                                            child: SizedBox(
                                              height: 18,
                                              width: 20,
                                            ),
                                          ),
                                          contentPadding:
                                              const EdgeInsets.all(8),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  text('Experience',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.80,
                                    height: MediaQuery.of(context).size.height *
                                        0.05,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppColors.buttontextColor,
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Color.fromRGBO(
                                              115, 115, 115, 0.25),
                                          offset: Offset(1, 3),
                                          blurRadius: 5,
                                          spreadRadius: 1,
                                        )
                                      ],
                                    ),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 17, right: 16),
                                            child: experiance,
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.20,
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.44,
                                            child: DropdownButton(
                                              isExpanded: true,
                                              underline: const SizedBox(),
                                              style: GoogleFonts.notoSans(
                                                fontSize: 10.sp,
                                                fontWeight: FontWeight.w500,
                                                color: const Color.fromRGBO(
                                                    34, 16, 0, 1),
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              value: experienceYear,
                                              icon: const Icon(
                                                Icons
                                                    .keyboard_arrow_down_rounded,
                                                size: 22,
                                                color: Colors.black54,
                                              ),
                                              items: items.map((String items) {
                                                return DropdownMenuItem(
                                                    value: items,
                                                    child: Text(
                                                      '${provider.myExperience}',
                                                    ));
                                              }).toList(),
                                              onChanged: (String? newvalue) {
                                                setState(() {
                                                  experienceYear = newvalue!;
                                                });
                                              },
                                            ),
                                          ),
                                        ]),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  text('Specialized In',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.80,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: AppColors.buttontextColor,
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Color.fromRGBO(
                                                115, 115, 115, 0.25),
                                            offset: Offset(1, 3),
                                            blurRadius: 5,
                                            spreadRadius: 1,
                                          )
                                        ],
                                      ),
                                      child: TextField(
                                        controller: specializedController,
                                        maxLength: 25,
                                        textAlign: TextAlign.center,
                                        cursorColor: Colors.black54,
                                        cursorHeight: 25,
                                        cursorWidth: 1,
                                        decoration: InputDecoration(
                                          counterText: '',
                                          hintText: '${provider.myWork}',
                                          prefixIcon: Padding(
                                            padding: const EdgeInsets.all(10),
                                            child: SizedBox(
                                                height: 18,
                                                width: 20,
                                                child: bspecialist),
                                          ),
                                          suffix: const Padding(
                                            padding: EdgeInsets.only(right: 12),
                                            child: SizedBox(
                                              height: 18,
                                              width: 20,
                                            ),
                                          ),
                                          contentPadding:
                                              const EdgeInsets.all(8),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  text('Availability Time',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.80,
                                    height: MediaQuery.of(context).size.height *
                                        0.05,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppColors.buttontextColor,
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Color.fromRGBO(
                                              115, 115, 115, 0.25),
                                          offset: Offset(1, 3),
                                          blurRadius: 5,
                                          spreadRadius: 1,
                                        )
                                      ],
                                    ),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 17, right: 16),
                                            child: available,
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.55,
                                            child: Center(
                                              child: Text(
                                                getTimeText(),
                                                style: GoogleFonts.notoSans(
                                                  fontSize: 10.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: const Color.fromRGBO(
                                                      34, 16, 0, 1),
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              onPressed: () {
                                                timepick();
                                              },
                                              icon: const Icon(
                                                Icons.access_time_sharp,
                                                size: 22,
                                                color: Colors.black54,
                                              ))
                                        ]),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  text('Cost per Hour / Service',
                                      color: AppColors.chekboxActiveColor,
                                      weight: FontWeight.w400,
                                      size: 11.sp),
                                  const SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.80,
                                    height: MediaQuery.of(context).size.height *
                                        0.05,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppColors.buttontextColor,
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Color.fromRGBO(
                                              115, 115, 115, 0.25),
                                          offset: Offset(1, 3),
                                          blurRadius: 5,
                                          spreadRadius: 1,
                                        )
                                      ],
                                    ),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 17, right: 16),
                                            child: cost,
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.55,
                                            child: Center(
                                              child: SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                child: Row(
                                                  children: [
                                                    SizedBox(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.10,
                                                      child: TextFormField(
                                                        controller:
                                                            costperhourController,
                                                        focusNode:
                                                            costperhourFocus,
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        onEditingComplete: () {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  costperhourFocus2);
                                                        },
                                                        cursorColor: AppColors
                                                            .buttoncolor,
                                                        style: GoogleFonts
                                                            .notoSans(
                                                          fontSize: 10.sp,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color: const Color
                                                              .fromRGBO(
                                                              34, 16, 0, 1),
                                                        ),
                                                        maxLength: 4,
                                                        decoration:
                                                            InputDecoration(
                                                          hintText: '100',
                                                          counterText: '',
                                                          errorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        15),
                                                            borderSide:
                                                                BorderSide.none,
                                                          ),
                                                          border:
                                                              const OutlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide
                                                                          .none),
                                                          contentPadding:
                                                              const EdgeInsets
                                                                  .symmetric(
                                                                  horizontal:
                                                                      1.0,
                                                                  vertical: 5),
                                                        ),
                                                      ),
                                                    ),
                                                    const Text(
                                                      '-',
                                                      style: TextStyle(
                                                          fontSize: 20),
                                                    ),
                                                    SizedBox(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.12,
                                                      child: TextFormField(
                                                        controller:
                                                            costperhourController2,
                                                        focusNode:
                                                            costperhourFocus2,
                                                        maxLength: 4,
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        cursorColor: AppColors
                                                            .buttoncolor,
                                                        style: GoogleFonts
                                                            .notoSans(
                                                          fontSize: 10.sp,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color: const Color
                                                              .fromRGBO(
                                                              34, 16, 0, 1),
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          counterText: '',
                                                          hintText: '200',
                                                          errorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        15),
                                                            borderSide:
                                                                BorderSide.none,
                                                          ),
                                                          border:
                                                              const OutlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide
                                                                          .none),
                                                          contentPadding:
                                                              const EdgeInsets
                                                                  .symmetric(
                                                                  horizontal:
                                                                      12.0,
                                                                  vertical: 7),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              onPressed: () {
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        costperhourFocus);
                                              },
                                              icon: const Icon(
                                                Icons.keyboard_alt_outlined,
                                                color: Colors.black54,
                                                size: 22,
                                              ))

                                          /*  TextButton(
                                        onPressed: () {
                                          timepick();
                                        },
                                        child: const Text('\u{20B9}'),
                                      )*/
                                        ]),
                                  ),
                                  SizedBox(
                                    height: 2.h,
                                  ),
                                ]),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.85,
                            height: MediaQuery.of(context).size.height * 0.06,
                            child: ElevatedButton(
                                style: const ButtonStyle(
                                    shape: MaterialStatePropertyAll(
                                        RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)))),
                                    backgroundColor: MaterialStatePropertyAll(
                                        AppColors.ActiveTabcolor)),
                                onPressed: () {},
                                child: text('Update',
                                    color: Colors.white,
                                    weight: FontWeight.w400,
                                    size: 17.sp)),
                          )
                        ],
                      ),
                    ),
                  )
                : Column(children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.95,
                      height: MediaQuery.of(context).size.height * 0.19,
                      decoration: const BoxDecoration(
                        color: AppColors.LocationbarColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4),
                          topRight: Radius.circular(4),
                        ),
                      ),
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        containershow = !containershow;
                                        imageDecreased = !imageDecreased;
                                      });
                                    },
                                    child: Ink(
                                      height: 25,
                                      width: 25,
                                      decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppColors.buttoncolor,
                                      ),
                                      child: const Icon(
                                        Icons.edit,
                                        color: Colors.white,
                                        size: 15,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 17,
                          ),
                          Consumer<ProviderScreen>(
                              builder: (context, myname, _) {
                            return text('${myname.myName}',
                                color: AppColors.overalltextcolor,
                                weight: FontWeight.w700,
                                size: 16.sp);
                          }),
                          const SizedBox(
                            height: 5,
                          ),
                          Consumer<ProviderScreen>(
                              builder: (context, myemail, _) {
                            return text('${myemail.myEmail}',
                                color: AppColors.commentlablecolor,
                                weight: FontWeight.w400,
                                size: 10.sp);
                          }),
                          const SizedBox(
                            height: 7,
                          ),
                          Consumer<ProviderScreen>(
                              builder: (context, mycenter, _) {
                            return text('${mycenter.myCenter}',
                                color: AppColors.overalltextcolor,
                                weight: FontWeight.w700,
                                size: 20.sp);
                          }),
                        ],
                      ),
                    ),
                    IndexedStack(
                      index: selectedPageIndex,
                      children: [
                        Profcom(
                          onSelectPage: (index) {
                            setState(() {
                              selectedPageIndex = index;
                            });
                          },
                        ),
                        Favourite(onSelectPage: (index) {
                          setState(() {
                            selectedPageIndex = index;
                          });
                        }),
                        Wallet(onSelectPage: (index) {
                          setState(() {
                            selectedPageIndex = index;
                          });
                        }),
                        Changepassword(onSelectPage: (index) {
                          setState(() {
                            selectedPageIndex = index;
                          });
                        }),
                      ],
                    ),
                  ]),
          ),
          AnimatedPositioned(
            top: imageDecreased ? 8 : 20,
            left: imageDecreased ? 160 : 135,
            right: imageDecreased ? 160 : 135,
            duration: const Duration(milliseconds: 300),
            child: Column(
              children: [
                Container(
                  height: imageDecreased ? 60.0 : 100.0,
                  width: imageDecreased ? 60.0 : 100.0,
                  decoration: const BoxDecoration(shape: BoxShape.circle),
                  child: SizedBox(
                    child: ClipOval(child: Image.asset('assets/IMG.png')),
                  ),
                )
              ],
            ),
          )
        ]));
  }
}











 /* (
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: profoptions.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        onSelectPage(index + 1);
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        height: MediaQuery.of(context).size.height * 0.02,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: AppColors.buttontextColor,
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(115, 115, 115, 0.25),
                              offset: Offset(1, 3),
                              blurRadius: 5,
                              spreadRadius: 1,
                            )
                          ],
                        ),
                        child: Row(
                          children: [
                            text('${profoptions[index]['name']}',
                                color: AppColors.commentlablecolor,
                                weight: FontWeight.w400,
                                size: 10.sp)
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ), */