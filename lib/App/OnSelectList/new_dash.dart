import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iyyan/App/dashboard.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class MenuDash extends StatefulWidget {
  final Function(int) onSelectScreen;

  const MenuDash({super.key, required this.onSelectScreen});

  @override
  State<MenuDash> createState() => _MenuDashState();
}

class _MenuDashState extends State<MenuDash> {
  List<dynamic> orderHistory = [
    {
      "Id": 1,
      "Name": 'Performance history',
      "image": 'assets/activehistory.svg',
      'activeimg': 'assets/activehis.svg',
    },
    {
      "Id": 2,
      "Name": 'Complete',
      "image": 'assets/activecomplete.svg',
      'activeimg': 'assets/activecom.svg'
    },
    {
      "Id": 3,
      "Name": 'Pending',
      "image": 'assets/activepending.svg',
      'activeimg': 'assets/activepend.svg'
    }
  ];
  List<Map<String, dynamic>> issueImage = [
    {'title': 'savari'},
    {'title': 'savari'},
  ];
  int _selectedPageIndex = 0;
  bool indexChange = false;

  void onSelectScreen(int screenId) {
    if (screenId == 1) {
      widget.onSelectScreen(1);
    } else if (screenId == 2) {}
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedPageIndex = index;
      indexChange = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: AppColors.LocationbarColor,
      child: Column(
        children: [
          Stack(children: [
            SizedBox(
              height: 15.h,
              width: double.infinity,
              child: Image.asset(
                'assets/banner2.png',
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
                width: 230,
                height: 115,
                child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: text(
                      'Tech Support Service Application',
                      color: AppColors.buttontextColor,
                      weight: FontWeight.w400,
                      size: 16.sp,
                      align: TextAlign.center,
                    ),
                  ),
                ))
          ]),
          SizedBox(height: 2.h),
          SizedBox(
            height: 12.h,
            width: MediaQuery.of(context).size.width,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(
                    orderHistory.length,
                    (index) => _selectedPageIndex == index &&
                            indexChange == true
                        ? Container(
                            height: double.infinity,
                            width: MediaQuery.of(context).size.width * 0.3,
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Material(
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          indexChange = false;
                                        });
                                      },
                                      child: Ink(
                                        height: 8.h,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.23,
                                        decoration: BoxDecoration(
                                          boxShadow: const [
                                            BoxShadow(
                                              color: Color.fromRGBO(
                                                  115, 115, 115, 0.25),
                                              offset: Offset(1, 3),
                                              blurRadius: 5,
                                              spreadRadius: 1,
                                            ),
                                          ],
                                          color: AppColors.ActiveTabcolor,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: Center(
                                          child: SvgPicture.asset(
                                            _selectedPageIndex == index ||
                                                    !indexChange
                                                ? '${orderHistory[index]['activeimg']}'
                                                : '${orderHistory[index]['image']}',
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                text('${orderHistory[index]['Name']}',
                                    color: AppColors.loginAndsigin,
                                    weight: FontWeight.w500,
                                    size: 7.sp)
                              ],
                            ),
                          )
                        : Container(
                            height: double.infinity,
                            width: MediaQuery.of(context).size.width * 0.3,
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Material(
                                    child: InkWell(
                                      onTap: () {
                                        _onItemTapped(index);
                                      },
                                      child: Ink(
                                        height: 8.h,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.23,
                                        decoration: BoxDecoration(
                                          boxShadow: const [
                                            BoxShadow(
                                              color: Color.fromRGBO(
                                                  115, 115, 115, 0.25),
                                              offset: Offset(1, 3),
                                              blurRadius: 5,
                                              spreadRadius: 1,
                                            )
                                          ],
                                          color: _selectedPageIndex == index
                                              ? AppColors.buttontextColor
                                              : AppColors.buttontextColor,
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(10)),
                                        ),
                                        child: Center(
                                          child: SvgPicture.asset(
                                            _selectedPageIndex == index
                                                ? orderHistory[index]
                                                    ['activeimg']
                                                : orderHistory[index]['image'],
                                            color: _selectedPageIndex == index
                                                ? AppColors.buttoncolor
                                                : null,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  '${orderHistory[index]['Name']}',
                                  style: TextStyle(
                                    color: AppColors.loginAndsigin,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 7.sp,
                                  ),
                                )
                              ],
                            ),
                          ))),
          ),
          Container(
            height: 54.h,
            width: double.infinity,
            alignment: Alignment.center,
            child: indexChange == false
                ? SizedBox(
                    height: 54.h,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            height: 5.h,
                            width: double.infinity,
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.only(left: 25),
                            child: text('Upcoming ticket',
                                color: AppColors.upcommingcolor,
                                weight: FontWeight.w600,
                                size: 12.sp),
                          ),
                          Container(
                            height: 50.h,
                            width: double.infinity,
                            padding: const EdgeInsets.all(5),
                            child: ListView.builder(
                                itemCount: 4,
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: [
                                      Container(
                                        margin:
                                            const EdgeInsets.only(bottom: 10),
                                        height: 11.h,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.9,
                                        decoration: const BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                color: Color.fromRGBO(
                                                    115, 115, 115, 0.25),
                                                offset: Offset(1, 3),
                                                blurRadius: 5,
                                                spreadRadius: 1,
                                              )
                                            ],
                                            color: AppColors.buttontextColor,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Column(
                                          children: [
                                            SizedBox(
                                              height: 7.h,
                                              width: double.infinity,
                                              child: Row(
                                                children: [
                                                  SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.15,
                                                    height: double.infinity,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        text('T-01',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp),
                                                        text('#T01',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp)
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.28,
                                                    height: double.infinity,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        text('Customer',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp),
                                                        text('Virat',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp)
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.28,
                                                    height: double.infinity,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        text('Issues',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp),
                                                        text('Screen repair',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp)
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.19,
                                                    height: double.infinity,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        text('Date',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp),
                                                        text('13-1-2024',
                                                            color: AppColors
                                                                .commentlablecolor,
                                                            weight:
                                                                FontWeight.w500,
                                                            size: 8.sp)
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              height: 3.h,
                                              width: double.infinity,
                                              alignment: Alignment.center,
                                              child: SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.5,
                                                height: double.infinity,
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                              Radius.circular(
                                                                  15)),
                                                      child: Material(
                                                        child: InkWell(
                                                          onTap: () {
                                                            bottomSheet();
                                                          },
                                                          child: Ink(
                                                            height: 2.3.h,
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width *
                                                                0.2,
                                                            decoration: const BoxDecoration(
                                                                color: AppColors
                                                                    .acceptbuttoncolor,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            15))),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                righttick,
                                                                text('Accept',
                                                                    color: AppColors
                                                                        .background_Color,
                                                                    weight:
                                                                        FontWeight
                                                                            .w400,
                                                                    size: 8.sp)
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                              Radius.circular(
                                                                  15)),
                                                      child: Material(
                                                        child: InkWell(
                                                          onTap: () {
                                                            widget
                                                                .onSelectScreen(
                                                                    1);
                                                          },
                                                          child: Ink(
                                                            height: 2.3.h,
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width *
                                                                0.2,
                                                            decoration: const BoxDecoration(
                                                                color: AppColors
                                                                    .declinecolor,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            15))),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                wrong,
                                                                text('Delete',
                                                                    color: AppColors
                                                                        .background_Color,
                                                                    weight:
                                                                        FontWeight
                                                                            .w400,
                                                                    size: 8.sp)
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 5,
                                            )
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                    ],
                                  );
                                }),
                          ),
                        ],
                      ),
                    ),
                  )
                : IndexedStack(
                    index: _selectedPageIndex,
                    children: [
                      History1(
                        onSelectPage1: (index) {
                          setState(() {
                            _selectedPageIndex = index;
                          });
                        },
                      ),
                      Complete1(onSelectPage1: (index) {
                        setState(() {
                          _selectedPageIndex = index;
                        });
                      }),
                      Pending1(
                        onSelectPage1: (index) {
                          setState(() {
                            _selectedPageIndex = index;
                          });
                        },
                        onSelectScreen: onSelectScreen,
                      ),
                    ],
                  ),
          )
        ],
      ),
    );
  }

  void bottomSheet() {
    showBottomSheet(
      context: context,
      builder: (context) => Container(
        height: 60.h,
        width: double.infinity,
        decoration: const BoxDecoration(
            color: AppColors.buttoncolor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        child: Column(
          children: [
            SizedBox(height: 0.5.h),
            GestureDetector(
              onTap: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                width: 10.h,
                height: 1.h,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        height: 2.5.h,
                        width: 2.5.h,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.acceptbuttoncolor),
                        child: const Icon(
                          Icons.check,
                          color: AppColors.buttoncolor,
                          size: 15,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      text('Accepting Ticket',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 8.sp)
                    ],
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        color: AppColors.background_Color,
                        borderRadius: BorderRadius.all(Radius.circular(3))),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: text('T01',
                          color: AppColors.buttoncolor,
                          weight: FontWeight.w500,
                          size: 6.sp),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      text('Narendran',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Name',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  ),
                  Column(
                    children: [
                      text('Screen Repair',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Issues',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      text('12.00am to 12.00pm',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Available Time',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  ),
                  Column(
                    children: [
                      text('Onsite',
                          color: AppColors.background_Color,
                          weight: FontWeight.w500,
                          size: 10.sp),
                      SizedBox(
                        height: 1.h,
                      ),
                      text('Service Mode',
                          color: AppColors.overalltextcolor,
                          weight: FontWeight.w500,
                          size: 10.sp)
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Material(
                      child: InkWell(
                        onTap: () {
                          widget.onSelectScreen(2);
                          Navigator.pop(context);
                        },
                        child: Ink(
                          height: 2.5.h,
                          width: 10.h,
                          decoration: BoxDecoration(
                              color: AppColors.chekboxActiveColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5, right: 5),
                            child: Row(
                              children: [
                                botlocation,
                                SizedBox(
                                  width: 1.h,
                                ),
                                text('Location',
                                    color: AppColors.background_Color,
                                    weight: FontWeight.w500,
                                    size: 8.sp),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Material(
                          child: InkWell(
                            onTap: () {
                              widget.onSelectScreen(15);
                              Navigator.pop(context);
                            },
                            child: Ink(
                              height: 2.5.h,
                              width: 3.h,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: AppColors.background_Color),
                              child: chatBlack,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Container(
                        height: 2.5.h,
                        width: 12.h,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: AppColors.acceptbuttoncolor),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 5, right: 5),
                          child: Row(
                            children: [
                              botCall,
                              SizedBox(
                                width: 1.w,
                              ),
                              text('9268708011',
                                  color: AppColors.background_Color,
                                  weight: FontWeight.w500,
                                  size: 8.sp),
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Column(
              children: [
                Container(
                  height: 12.h,
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      const BoxDecoration(color: AppColors.background_Color),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15, top: 5),
                            child: text('Issue description',
                                color: AppColors.lightblack,
                                weight: FontWeight.w500,
                                size: 10.sp),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: text(
                              'Screen Problem means an error condition that is repeatable ',
                              color: AppColors.lightblack,
                              weight: FontWeight.w500,
                              size: 10.sp),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 23.5.h,
              width: MediaQuery.of(context).size.width,
              decoration:
                  const BoxDecoration(color: AppColors.LocationbarColor),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 6,
                      mainAxisExtent: 150),
                  itemCount: issueImage.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey),
                      child: Center(child: Text(issueImage[index]['title'])),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
