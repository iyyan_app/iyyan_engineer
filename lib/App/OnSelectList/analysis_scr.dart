import 'package:flutter/material.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import '../../CommenScreen/AppColors.dart';
import '../../CommenScreen/CommenWidget.dart';

class AnalysisScr extends StatefulWidget {
  final Function onSelectScreen;
  const AnalysisScr({super.key, required this.onSelectScreen});

  @override
  State<AnalysisScr> createState() => _AnalysisScrState();
}

class _AnalysisScrState extends State<AnalysisScr> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Container(
            height: 50.h,
            width: MediaQuery.of(context).size.width,
            color: AppColors.LocationbarColor,
            child: Column(
              children: [
                SizedBox(height: 4.h),
                GestureDetector(
                  onTap: () {
                    widget.onSelectScreen(3);
                  },
                  child: text('Timer',
                      color: AppColors.cancelBlack,
                      weight: FontWeight.w500,
                      size: 19.sp),
                ),
                SizedBox(height: 3.h),
                SizedBox(
                  height: 35.h,
                  width: MediaQuery.of(context).size.width * 0.60,
                  child: Image.asset(
                    'assets/sandClock.gif',
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 4.h),
          Column(
            children: [
              Consumer<ProviderScreen>(
                builder: (context, provider, _) {
                  return text(
                    '${provider.timerString} min',
                    color: AppColors.commentlablecolor,
                    weight: FontWeight.w700,
                    size: 20.sp,
                  );
                },
              ),
              SizedBox(height: 16.h),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
                width: MediaQuery.of(context).size.width * 0.75,
                child: ElevatedButton(
                  onPressed: () {
                    widget.onSelectScreen(5);
                    final provider =
                        Provider.of<ProviderScreen>(context, listen: false);
                    provider.stopAnalys();
                  },
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(AppColors.ActiveTabcolor),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ),
                  child: text(
                    'Analysis',
                    color: AppColors.background_Color,
                    weight: FontWeight.w400,
                    size: 18.sp,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
