import 'package:flutter/material.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:sizer/sizer.dart';

import '../../CommenScreen/Allassets.dart';
import '../../CommenScreen/CommenWidget.dart';

class NotificationScr extends StatefulWidget {
  final VoidCallback onBack;

  const NotificationScr({required this.onBack, super.key});

  @override
  State<NotificationScr> createState() => _NotificationScrState();
}

class _NotificationScrState extends State<NotificationScr> {
  List<Map<String, dynamic>> notifications = [
    {
      'day': 'Today',
      'title': 'when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      'subtitle': 'Today',
      'image': const AssetImage('assets/IMG.png'),
    },
    {
      'day': 'Yesterday',
      'title': 'when an unknown printer took a galley of type and scrambled it to It has survived not only five centuries,',
      'subtitle': 'Yesterday',
      'image': const AssetImage('assets/IMG.png'),
    },
    {
      'day': 'Yesterday',
      'title': 'when an unknown printer took a galley of type and scrambled it to ',
      'subtitle': 'Yesterday',
      'image': const AssetImage('assets/IMG.png'),
    },
    {
      'day': '21/01/2024',
      'title': 'when an unknown printer',
      'subtitle': '21/01/2024',
      'image': const AssetImage('assets/IMG.png'),
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(gradient: AppColors.linear1),
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              oval1,
              oval2,
            ],
          ),
          IconButton(
            onPressed: widget.onBack,
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.10,
          ),
          Positioned(
            top: 35,
            left: 135,
            right: 135,
            child: text(
              'Notification',
              color: AppColors.background_Color,
              weight: FontWeight.w400,
              size: 16.sp,
            ),
          ),
          Positioned(
            top: 70,
            left: 10,
            right: 10,
            bottom: 0,
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(6),
                  topRight: Radius.circular(6),
                ),
                color: AppColors.background_Color,
              ),
              child: ListView.builder(
                itemCount: notifications.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 3, left: 15),
                          child: text(
                            notifications[index]['day'].toString(),
                            color: AppColors.commonblack,
                            weight: FontWeight.w500,
                            size: 9.sp,
                          ),
                        ),
                      ),
                      ListTile(
                        leading: Container(
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(shape: BoxShape.circle),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Image.asset('assets/IMG.png'),
                          ),
                        ),
                        title: text(
                          '${notifications[index]['title']}',
                          color: AppColors.commonblack,
                          weight: FontWeight.w500,
                          size: 11.sp,
                        ),
                        subtitle: Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: text(
                            '${notifications[index]['subtitle']}',
                            color: AppColors.notifysub,
                            weight: FontWeight.w500,
                            size: 10.sp,
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 1,
                        color: AppColors.commonblack,
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
