import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class PaymentSplash extends StatefulWidget {
  final Function onSelectScreen;
  const PaymentSplash({super.key, required this.onSelectScreen});

  @override
  State<PaymentSplash> createState() => _PaymentSplashState();
}

class _PaymentSplashState extends State<PaymentSplash> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 1,
        width: MediaQuery.of(context).size.width * 1,
        color: AppColors.LocationbarColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                height: 40.h,
                width: MediaQuery.of(context).size.width * 0.90,
                child: paySplash),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () {
                widget.onSelectScreen(16);
              },
              child: text('Payment',
                  color: AppColors.commonblack,
                  weight: FontWeight.w500,
                  size: 20.sp),
            )
          ],
        ));
  }
}
