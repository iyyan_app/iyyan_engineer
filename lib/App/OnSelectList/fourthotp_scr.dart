import 'package:flutter/material.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class FourthOtp extends StatefulWidget {
  final Function onSelectScreen;
  const FourthOtp({super.key, required this.onSelectScreen});

  @override
  State<FourthOtp> createState() => _FourthOtpState();
}

class _FourthOtpState extends State<FourthOtp> {
  final firstNumber = TextEditingController();
  final secondNumber = TextEditingController();
  final thirdNumber = TextEditingController();
  final fourthNumber = TextEditingController();

  final firstFocus = FocusNode();
  final secondFocus = FocusNode();
  final thirdFocus = FocusNode();
  final fourthFocus = FocusNode();

  @override
  void dispose() {
    firstNumber.dispose();
    secondNumber.dispose();
    thirdNumber.dispose();
    fourthNumber.dispose();
    firstFocus.dispose();
    secondFocus.dispose();
    thirdFocus.dispose();
    fourthFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderScreen>(context, listen: false);
    return SizedBox(
      width: MediaQuery.of(context).size.width * 1,
      height: MediaQuery.of(context).size.height * 1,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 50.h,
              width: MediaQuery.of(context).size.width,
              color: AppColors.LocationbarColor,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 4.h,
                    ),
                    GestureDetector(
                      onTap: () {
                        widget.onSelectScreen(8);
                      },
                      child: text('OTP Verification',
                          color: AppColors.cancelBlack,
                          weight: FontWeight.w500,
                          size: 19.sp),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: text(
                          'Enter the 4 digits code that you received on your Phone number',
                          align: TextAlign.center,
                          color: AppColors.lightblack,
                          weight: FontWeight.w400,
                          size: 12.sp),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        myInputBox4(
                            context, firstNumber, firstFocus, secondFocus),
                        myInputBox4(
                            context, secondNumber, secondFocus, thirdFocus),
                        myInputBox4(
                            context, thirdNumber, thirdFocus, fourthFocus),
                        myInputBox4(context, fourthNumber, fourthFocus, null)
                      ],
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      clockIcon,
                      const SizedBox(
                        width: 10,
                      ),
                      Consumer<ProviderScreen>(
                          builder: (context, prodValue4, child) {
                        return text(
                            '00:${'${prodValue4.seconds4}'.padLeft(2, '0')}',
                            color: AppColors.lightblack,
                            weight: FontWeight.w600,
                            size: 12.sp);
                      }),
                    ]),
                    SizedBox(
                      height: 2.h,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              firstNumber.clear();
                              secondNumber.clear();
                              thirdNumber.clear();
                              fourthNumber.clear();
                              if (provider.isTimer6 == false) {
                                provider.restartTimer4();
                              }
                            });
                          },
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.45,
                            height: 4.h,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  recentIcon,
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Consumer<ProviderScreen>(
                                      builder: (context, otpmessage4, _) {
                                    return text(
                                        otpmessage4.isTimer6
                                            ? 'Enter the Code'
                                            : 'Resend the Code',
                                        color: AppColors.lightblack,
                                        weight: FontWeight.w600,
                                        size: 12.sp);
                                  }),
                                ]),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 23.h,
            ),
            SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
                width: MediaQuery.of(context).size.width * 0.75,
                child: ElevatedButton(
                    onPressed: () {
                      widget.onSelectScreen(10);
                      provider.timer6!.cancel();
                      provider.startAnalys3();
                    },
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(AppColors.ActiveTabcolor),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)))),
                    child: text('Continue',
                        color: AppColors.background_Color,
                        weight: FontWeight.w400,
                        size: 18.sp))),
          ],
        ),
      ),
    );
  }
}

Widget myInputBox4(BuildContext context, TextEditingController controller,
    FocusNode currentFocus, FocusNode? nextFocus) {
  return Container(
    height: 6.5.h,
    width: MediaQuery.of(context).size.width * 0.09,
    decoration: BoxDecoration(
      color: AppColors.background_Color,
      border: Border.all(width: 2, color: AppColors.buttoncolor),
      borderRadius: const BorderRadius.all(Radius.circular(15)),
    ),
    child: TextField(
      controller: controller,
      focusNode: currentFocus,
      maxLength: 1,
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      style: TextStyle(fontSize: 24.sp),
      decoration: const InputDecoration(
        counterText: '',
        contentPadding: EdgeInsets.all(0),
        border: OutlineInputBorder(borderSide: BorderSide.none),
      ),
      onChanged: (value) {
        if (value.length == 1) {
          if (nextFocus != null) {
            FocusScope.of(context).requestFocus(nextFocus);
          } else {
            currentFocus.unfocus();
          }
        }
      },
    ),
  );
}
