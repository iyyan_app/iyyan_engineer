import 'package:flutter/material.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class Analysis2Scr extends StatefulWidget {
  final Function onSelectScreen;
  const Analysis2Scr({super.key, required this.onSelectScreen});

  @override
  State<Analysis2Scr> createState() => _Analysis2ScrState();
}

class _Analysis2ScrState extends State<Analysis2Scr> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderScreen>(context, listen: false);
    return SizedBox(
      height: MediaQuery.of(context).size.height * 1,
      width: MediaQuery.of(context).size.width * 1,
      child: Column(
        children: [
          Container(
            height: 50.h,
            width: MediaQuery.of(context).size.width,
            color: AppColors.LocationbarColor,
            child: Column(
              children: [
                SizedBox(
                  height: 4.h,
                ),
                GestureDetector(
                  onTap: () {
                    widget.onSelectScreen(6);
                  },
                  child: text('Timer',
                      color: AppColors.cancelBlack,
                      weight: FontWeight.w500,
                      size: 19.sp),
                ),
                SizedBox(
                  height: 3.h,
                ),
                SizedBox(
                    height: 35.h,
                    width: MediaQuery.of(context).size.width * 0.60,
                    child: provider.playpause
                        ? Image.asset(
                            'assets/sandclockpng.png',
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            'assets/sandClock.gif',
                            fit: BoxFit.cover,
                          ))
              ],
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Material(
              child: InkWell(
                  onTap: () {
                    setState(() {
                      provider.playpause = !provider.playpause;
                    });
                  },
                  child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: provider.playpause ? playButton : pauseButton)),
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          Column(
            children: [
              Consumer<ProviderScreen>(builder: (context, timestring2, _) {
                return text('${timestring2.timerString2} min',
                    color: AppColors.commentlablecolor,
                    weight: FontWeight.w700,
                    size: 20.sp);
              }),
              SizedBox(
                height: 4.h,
              ),
              SizedBox(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: ElevatedButton(
                      onPressed: () {
                        widget.onSelectScreen(11);
                      },
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              AppColors.ActiveTabcolor),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)))),
                      child: text('Postponed',
                          color: AppColors.background_Color,
                          weight: FontWeight.w400,
                          size: 18.sp))),
              SizedBox(
                height: 2.h,
              ),
              SizedBox(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: ElevatedButton(
                      onPressed: () {
                        widget.onSelectScreen(8);
                        provider.startTimer3();
                        provider.stopAnalys2();
                      },
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              AppColors.ActiveTabcolor),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)))),
                      child: text('Continue',
                          color: AppColors.background_Color,
                          weight: FontWeight.w400,
                          size: 18.sp))),
            ],
          ),
        ],
      ),
    );
  }
}
