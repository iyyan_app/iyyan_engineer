/*import 'package:flutter/material.dart';
import 'package:iyyan/App/dashboard.dart';
import 'package:map_location_picker/map_location_picker.dart';

class LocationSearchWidget extends StatefulWidget {
  final Function onSelectScreen;
  const LocationSearchWidget({super.key, required this.onSelectScreen});

  @override
  _LocationSearchWidgetState createState() => _LocationSearchWidgetState();
}

class _LocationSearchWidgetState extends State<LocationSearchWidget> {
  final TextEditingController _searchController = TextEditingController();
  final GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: apiKey);

  List<Prediction> _predictions = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: _searchController,
          decoration: const InputDecoration(
            labelText: 'Search',
            prefixIcon: Icon(Icons.search),
          ),
          onChanged: _onSearchTextChanged,
        ),
      ),
      body: ListView.builder(
        itemCount: _predictions.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text('${_predictions[index].description}'),
            onTap: () {
              // Handle selection of location prediction
              _onLocationSelected('${_predictions[index].placeId}');
            },
          );
        },
      ),
    );
  }

  void _onSearchTextChanged(String query) async {
    if (query.isEmpty) {
      setState(() {
        _predictions.clear();
      });
      return;
    }

    try {
      PlacesAutocompleteResponse response = await _places.autocomplete(query);
      print(
          'Autocomplete Response: ${response.toJson()}'); // Print autocomplete response
      setState(() {
        _predictions = response.predictions;
      });
    } catch (e) {
      print(
          'Error fetching autocomplete predictions: $e'); // Print error message
      // Handle error condition if needed
    }
  }

  void _onLocationSelected(String placeId) async {
    PlacesDetailsResponse details = await _places.getDetailsByPlaceId(placeId);
    // Handle the selected location details (e.g., coordinates, address)
    // For example, you can use details.result.geometry.location to get the coordinates
    print('Selected location: ${details.result.name}');
  }
}
*/


/*import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as location_package;

class Mappage extends StatefulWidget {
  final Function onSelectScreen;
  const Mappage({super.key, required this.onSelectScreen});

  @override
  State<Mappage> createState() => _MappageState();
}

class _MappageState extends State<Mappage> {
  final Completer<GoogleMapController> _controller = Completer();
  LatLng? destination;

  List<LatLng> polylineCoordinates = [];
  location_package.LocationData? currentLocation;

  BitmapDescriptor sourceIcon = BitmapDescriptor.defaultMarker;
  BitmapDescriptor destinationIcon = BitmapDescriptor.defaultMarker;
  BitmapDescriptor currentLocationIcon = BitmapDescriptor.defaultMarker;
  BitmapDescriptor selectedLocationIcon = BitmapDescriptor.defaultMarker;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
    _setCustomMarkerIcons();
  }

  Future<void> _getCurrentLocation() async {
    location_package.Location location = location_package.Location();

    try {
      currentLocation = await location.getLocation();
      setState(() {});
    } catch (e) {
      print("Error getting location: $e");
      return;
    }

    location.onLocationChanged.listen((newLoc) {
      setState(() {
        currentLocation = newLoc;
      });

      _controller.future.then((googleMapController) {
        googleMapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              zoom: 13.5,
              target: LatLng(newLoc.latitude!, newLoc.longitude!),
            ),
          ),
        );
      });
    });
  }

  Future<void> _getPolylinePoints(LatLng destination) async {
    PolylinePoints polylinePoints = PolylinePoints();
    try {
      PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        'AIzaSyCGShAceyIm1LHL2mLja0eKCKDjoZV2RzY', // Make sure this API key is valid and has the necessary permissions
        PointLatLng(currentLocation!.latitude!, currentLocation!.longitude!),
        PointLatLng(destination.latitude, destination.longitude),
      );

      if (result.status == 'OK') {
        setState(() {
          polylineCoordinates = result.points
              .map((point) => LatLng(point.latitude, point.longitude))
              .toList();
        });
      } else {
        print("Failed to get route: ${result.status}");
      }
    } catch (e) {
      print("Error getting polyline points: $e");
    }
  }

  Future<void> _setCustomMarkerIcons() async {
    try {
      sourceIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(48, 48)),
        'assets/IMG.png',
      );
      destinationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(48, 48)),
        'assets/IMG.png',
      );
      currentLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(48, 48)),
        'assets/IMG.png',
      );
      selectedLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(48, 48)),
        'assets/IMG.png',
      );
    } catch (e) {
      print("Error setting custom marker icons: $e");
    }
  }

  void _onMapTap(LatLng position) {
    setState(() {
      destination = position;
      polylineCoordinates.clear(); // Clear previous polyline
    });
  }

  void _onDrawRouteButtonPressed() {
    if (destination != null && currentLocation != null) {
      _getPolylinePoints(destination!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tracking for Location'),
      ),
      body: currentLocation == null
          ? const Center(child: CircularProgressIndicator())
          : Stack(
              children: [
                GoogleMap(
                  initialCameraPosition: CameraPosition(
                    target: LatLng(currentLocation!.latitude!,
                        currentLocation!.longitude!),
                    zoom: 13.5,
                  ),
                  polylines: {
                    Polyline(
                      polylineId: const PolylineId('route'),
                      points: polylineCoordinates,
                      color: Colors.red,
                      width: 6,
                    ),
                  },
                  markers: {
                    Marker(
                      markerId: const MarkerId("currentLocation"),
                      icon: currentLocationIcon,
                      position: LatLng(currentLocation!.latitude!,
                          currentLocation!.longitude!),
                    ),
                    if (destination != null)
                      Marker(
                        markerId: const MarkerId("destination"),
                        icon: selectedLocationIcon,
                        position: destination!,
                      ),
                  },
                  onMapCreated: (mapController) {
                    _controller.complete(mapController);
                  },
                  onTap: _onMapTap,
                ),
                Positioned(
                  bottom: 50,
                  left: 10,
                  child: ElevatedButton(
                    onPressed: _onDrawRouteButtonPressed,
                    child: const Text('Draw Route'),
                  ),
                ),
              ],
            ),
    );
  }
}
*/