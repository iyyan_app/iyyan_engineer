import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:sizer/sizer.dart';

import '../../CommenScreen/Allassets.dart';
import '../../CommenScreen/CommenWidget.dart';

class QRScreen extends StatefulWidget {
  final Function onSelectScreen;
  const QRScreen({super.key, required this.onSelectScreen});

  @override
  State<QRScreen> createState() => _QRScreenState();
}

double? ratings;

class _QRScreenState extends State<QRScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 1,
        width: MediaQuery.of(context).size.width * 1,
        color: AppColors.LocationbarColor,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 4.h,
              ),
              GestureDetector(
                onTap: () {
                  widget.onSelectScreen(0);
                },
                child: text('QR code Scan',
                    color: AppColors.commonblack,
                    weight: FontWeight.w500,
                    size: 20.sp),
              ),
              SizedBox(
                height: 10.h,
              ),
              Container(
                height: 48.h,
                width: MediaQuery.of(context).size.width * 0.75,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: AppColors.background_Color,
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) => const SuccesPay());
                      },
                      child: SizedBox(
                          height: 30.h,
                          width: MediaQuery.of(context).size.width * 0.55,
                          child: qrImage),
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    text('Scan to Pay',
                        color: AppColors.commonblack,
                        weight: FontWeight.w500,
                        size: 10.sp),
                    SizedBox(
                      height: 2.h,
                    ),
                    text('UPI ID: savarioksbi',
                        color: AppColors.commonblack,
                        weight: FontWeight.w500,
                        size: 10.sp),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class SuccesPay extends StatefulWidget {
  const SuccesPay({super.key});

  @override
  State<SuccesPay> createState() => _SuccesPayState();
}

class _SuccesPayState extends State<SuccesPay> {
  final commentsControoler = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 33.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppColors.buttoncolor),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Icon(
                  Icons.check_circle,
                  color: AppColors.acceptbuttoncolor,
                ),
                Column(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.60,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 1.h),
                            text('Payment successfully!',
                                color: AppColors.background_Color,
                                weight: FontWeight.w600,
                                size: 13.sp),
                            SizedBox(height: 1.5.h),
                            text('Your feedback and rating ',
                                color: AppColors.yourFeedback,
                                weight: FontWeight.w400,
                                size: 10.sp),
                            SizedBox(height: 3.h),
                          ]),
                    ),
                    RatingBar.builder(
                        initialRating: ratings != null ? ratings! : 3,
                        minRating: 1,
                        itemSize: 20,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding:
                            const EdgeInsets.symmetric(horizontal: 1.0),
                        unratedColor: AppColors.demoCardcolor,
                        itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: AppColors.starColor,
                            ),
                        onRatingUpdate: (rating) {
                          setState(() {
                            ratings = rating;
                          });
                        }),
                    SizedBox(height: 1.h),
                    text(ratings != null ? '$ratings rating' : '3.0 rating',
                        color: AppColors.yourFeedback,
                        weight: FontWeight.w400,
                        size: 8.sp),
                    SizedBox(height: 2.h),
                    Container(
                      decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.background_Color)),
                      width: MediaQuery.of(context).size.width * 0.60,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: TextField(
                          controller: commentsControoler,
                          keyboardType: TextInputType.name,
                          style: const TextStyle(
                              color: AppColors.background_Color),
                          minLines: 1,
                          maxLines: 2,
                          decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.only(left: 10, top: 3),
                              labelText: 'Add your Comments',
                              labelStyle: GoogleFonts.notoSans(
                                  color: AppColors.yourFeedback,
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w400),
                              border: const OutlineInputBorder(
                                  borderSide: BorderSide.none)),
                        ),
                      ),
                    ),
                    SizedBox(height: 3.h),
                    SizedBox(
                      height: 3.h,
                      child: ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: text('SUBMIT',
                              color: AppColors.completegreen,
                              weight: FontWeight.w600,
                              size: 8.sp)),
                    )
                  ],
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Icon(
                        Icons.close,
                        color: AppColors.yourFeedback,
                      ),
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
