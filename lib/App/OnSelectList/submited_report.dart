import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class SubmittedScr extends StatefulWidget {
  final Function onSelectScreen;
  const SubmittedScr({super.key, required this.onSelectScreen});

  @override
  State<SubmittedScr> createState() => _SubmittedScrState();
}

class _SubmittedScrState extends State<SubmittedScr>
    with TickerProviderStateMixin {
  TextEditingController analyseReason = TextEditingController();
  final ImagePicker _imagePicker = ImagePicker();
  File? imageFile2;
  AnimationController? controller;
  AnimationController? controller2;
  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
    controller2 = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  void dispose() {
    controller!.dispose();
    controller!.dispose();
    super.dispose();
  }

  Future<void> _openCamera() async {
    final pickedImage =
        await _imagePicker.pickImage(source: ImageSource.camera);
    if (pickedImage != null) {
      setState(() {
        imageFile2 = File(pickedImage.path);
      });
    }
  }

  Future<void> _openGallery() async {
    final pickedImage =
        await _imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      setState(() {
        imageFile2 = File(pickedImage.path);
      });
    }
  }

  void showoption() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _openCamera();
                          Navigator.pop(context);
                        },
                        child: SizedBox(
                          height: 40,
                          width: 40,
                          child: Image.asset(
                            'assets/camera.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      const Text(
                        'Camera',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _openGallery();
                          Navigator.pop(context);
                        },
                        child: SizedBox(
                          height: 40,
                          width: 40,
                          child: Image.asset(
                            'assets/file.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      const Text(
                        'File',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  List<dynamic> issuelist = [
    {'title': 'Slow Performance'},
    {'title': 'Overheating'},
    {'title': 'Battery Not Charging'},
    {'title': 'Screen Issues'},
    {'title': 'Hardware Failure'},
    {'title': 'Virus and Malware Infections'},
    {'title': 'Wi-Fi Connection Problems'},
    {'title': 'Hard Ddrive Failures'},
  ];
  List<dynamic> partslist = [
    {'title': 'Processor (CPU)'},
    {'title': 'Memory (RAM)'},
    {'title': 'Storage Drive'},
    {'title': 'Motherboard'},
    {'title': 'Graphics Processing Unit (GPU)'},
    {'title': 'Display Screen'},
    {'title': 'Keyboard and Touchpad'},
    {'title': 'Battery'},
    {'title': 'Cooling System'},
  ];
  String selectedissue = '';
  String selectedparts = '';

  bool showIssues = false;
  bool showParts = false;
  TextEditingController serialNumber = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 1,
      width: MediaQuery.of(context).size.width * 1,
      decoration: const BoxDecoration(color: AppColors.LocationbarColor),
      child: Column(
        children: [
          SizedBox(
            height: 3.h,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: GestureDetector(
              onTap: () {
                widget.onSelectScreen(12);
              },
              child: Align(
                alignment: Alignment.topLeft,
                child: text('Submit working Process',
                    color: AppColors.cancelBlack,
                    weight: FontWeight.w500,
                    size: 12.sp),
              ),
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, right: 40),
            child: SingleChildScrollView(
              child: Stack(children: [
                Column(
                  children: [
                    SizedBox(
                      height: 3.h,
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: text('Issues',
                          color: AppColors.lightblack,
                          weight: FontWeight.w500,
                          size: 10.sp),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Container(
                      height: 6.h,
                      width: MediaQuery.of(context).size.width * 0.80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: AppColors.background_Color),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              height: 6.h,
                              width: MediaQuery.of(context).size.width * 0.68,
                              child: Center(
                                child: text(
                                    selectedissue.isEmpty
                                        ? 'Issues'
                                        : selectedissue,
                                    color: AppColors.cancelBlack,
                                    weight: FontWeight.w400,
                                    size: 12.sp),
                              ),
                            ),
                          ),
                          GestureDetector(
                              onTap: () {
                                setState(() {
                                  showIssues = !showIssues;
                                  showParts = false;
                                  controller!.forward(from: 0.0);
                                });
                              },
                              child: RotationTransition(
                                turns: Tween(begin: 0.0, end: 0.5)
                                    .animate(controller!),
                                child: Icon(showIssues
                                    ? Icons.keyboard_arrow_down_rounded
                                    : Icons.keyboard_arrow_up_rounded),
                              )),
                          const SizedBox(
                            width: 1,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: text('Additional parts',
                          color: AppColors.lightblack,
                          weight: FontWeight.w500,
                          size: 10.sp),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Container(
                      height: 6.h,
                      width: MediaQuery.of(context).size.width * 0.80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: AppColors.background_Color),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              height: 6.h,
                              width: MediaQuery.of(context).size.width * 0.68,
                              child: Center(
                                child: text(
                                    selectedparts.isEmpty
                                        ? 'Additional Parts'
                                        : selectedparts,
                                    color: AppColors.cancelBlack,
                                    weight: FontWeight.w400,
                                    size: 12.sp),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                showParts = !showParts;
                                showIssues = false;
                                controller2!.forward(from: 0.0);
                              });
                            },
                            child: RotationTransition(
                              turns: Tween(begin: 0.0, end: 0.5)
                                  .animate(controller2!),
                              child: Icon(showParts
                                  ? Icons.keyboard_arrow_down_rounded
                                  : Icons.keyboard_arrow_up_rounded),
                            ),
                          ),
                          const SizedBox(
                            width: 1,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: text('Serial No',
                          color: AppColors.lightblack,
                          weight: FontWeight.w500,
                          size: 10.sp),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Container(
                      height: 6.h,
                      width: MediaQuery.of(context).size.width * 0.80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: AppColors.background_Color),
                      child: TextField(
                        controller: serialNumber,
                        textAlign: TextAlign.center,
                        decoration: const InputDecoration(
                            hintText: 'Add Serial Number',
                            contentPadding: EdgeInsets.all(0),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none)),
                      ),
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    Container(
                      height: 15.h,
                      width: 45.h,
                      decoration: BoxDecoration(
                        color: AppColors.background_Color,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextField(
                        controller: analyseReason,
                        keyboardType: TextInputType.name,
                        maxLines: null,
                        maxLengthEnforcement: MaxLengthEnforcement.enforced,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(
                              left: 10,
                            ),
                            labelText: 'Reason',
                            labelStyle: GoogleFonts.notoSans(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.w400),
                            hintText: '',
                            hintStyle: GoogleFonts.notoSans(
                                color: Colors.black45,
                                fontSize: 12,
                                fontWeight: FontWeight.w400),
                            border: const UnderlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: const UnderlineInputBorder(
                                borderSide: BorderSide.none)),
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              showoption();
                            },
                            child: SizedBox(
                              height: 3.h,
                              width: MediaQuery.of(context).size.width * 0.30,
                              child: Row(
                                children: [
                                  attachment,
                                  const SizedBox(
                                    width: 3,
                                  ),
                                  text('Attachment',
                                      color: AppColors.yourLocation,
                                      weight: FontWeight.w400,
                                      size: 11.sp)
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                      width: MediaQuery.of(context).size.width * 0.20,
                      child: imageFile2 != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: GestureDetector(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) => Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            height: 60.sp,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.60,
                                            child: Dialog(
                                                child: imageFile2 != null
                                                    ? ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Image.file(
                                                              imageFile2!),
                                                        ))
                                                    : const Text('')),
                                          ));
                                },
                                child: Image.file(
                                  imageFile2!,
                                  fit: BoxFit.cover,
                                  //    fit: BoxFit.fill,
                                ),
                              ),
                            )
                          : const Center(child: Text('No Image')),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    SizedBox(
                        height: MediaQuery.of(context).size.height * 0.06,
                        width: MediaQuery.of(context).size.width * 0.75,
                        child: ElevatedButton(
                            onPressed: () {
                              widget.onSelectScreen(13);
                            },
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    AppColors.ActiveTabcolor),
                                shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8)))),
                            child: text('Submit',
                                color: AppColors.background_Color,
                                weight: FontWeight.w400,
                                size: 18.sp))),
                  ],
                ),
                showIssues
                    ? Positioned(
                        top: 95,
                        left: 40,
                        child: Container(
                          height: 40.h,
                          width: MediaQuery.of(context).size.width * 0.70,
                          decoration: BoxDecoration(
                              color: AppColors.background_Color,
                              borderRadius: BorderRadius.circular(8)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListView.builder(
                              itemCount: issuelist.length,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                return SizedBox(
                                  height: 6.h,
                                  child: RadioListTile(
                                      title: Text(issuelist[index]['title']),
                                      value: issuelist[index]['title'],
                                      groupValue: selectedissue,
                                      fillColor: MaterialStateColor.resolveWith(
                                          (states) => AppColors.ActiveTabcolor),
                                      onChanged: (value) {
                                        setState(() {
                                          selectedissue =
                                              value?.toString() ?? '';
                                          Future.delayed(
                                              const Duration(milliseconds: 500),
                                              () {
                                            setState(() {
                                              showIssues = !showIssues;
                                            });
                                          });
                                        });
                                      }),
                                );
                              },
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                showParts
                    ? Positioned(
                        top: 175,
                        left: 40,
                        child: Container(
                          height: 40.h,
                          width: MediaQuery.of(context).size.width * 0.70,
                          decoration: BoxDecoration(
                              color: AppColors.background_Color,
                              borderRadius: BorderRadius.circular(8)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListView.builder(
                              itemCount: partslist.length,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                return SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 6.h,
                                        child: RadioListTile(
                                            title:
                                                Text(partslist[index]['title']),
                                            value: partslist[index]['title'],
                                            groupValue: selectedparts,
                                            fillColor:
                                                MaterialStateColor.resolveWith(
                                                    (states) => AppColors
                                                        .ActiveTabcolor),
                                            onChanged: (value) {
                                              setState(() {
                                                selectedparts =
                                                    value?.toString() ?? '';
                                                Future.delayed(
                                                    const Duration(
                                                        milliseconds: 500), () {
                                                  setState(() {
                                                    showParts = !showParts;
                                                  });
                                                });
                                              });
                                            }),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
