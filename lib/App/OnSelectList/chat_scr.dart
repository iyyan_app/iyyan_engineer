import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:mime/mime.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sizer/sizer.dart';
import 'package:uuid/uuid.dart';

class ChatScr extends StatefulWidget {
  final Function onSelectScreen;
  const ChatScr({super.key, required this.onSelectScreen});

  @override
  State<ChatScr> createState() => _ChatScrState();
}

class _ChatScrState extends State<ChatScr> {
  final picker2 = ImagePicker();
  File? selectedImage2;
  bool isImageVisible2 = true;
  get http => null;

  final List<types.Message> _messages = [];
  final _user = const types.User(
    id: '82091008-a484-4a89-ae75-a22bf8d6f3ac',
  );

  @override
  void initState() {
    super.initState();
    _loadMessages();
  }

  void _addMessage(types.Message message) {
    setState(() {
      _messages.insert(0, message);
    });
  }

  void _handleAttachmentPressed() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) => SafeArea(
        child: SizedBox(
          height: 100,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                const SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    IconButton(
                        onPressed: () async {
                          final result = await ImagePicker().pickImage(
                            imageQuality: 70,
                            maxWidth: 1440,
                            source: ImageSource.gallery,
                          );

                          if (result != null) {
                            final bytes = await result.readAsBytes();
                            final image = await decodeImageFromList(bytes);

                            final message = types.ImageMessage(
                              author: _user,
                              createdAt: DateTime.now().millisecondsSinceEpoch,
                              height: image.height.toDouble(),
                              id: const Uuid().v4(),
                              name: result.name,
                              size: bytes.length,
                              uri: result.path,
                              width: image.width.toDouble(),
                            );

                            _addMessage(message);
                          }
                        },
                        icon: const Icon(
                          Icons.image,
                          size: 30,
                        )),
                    const Text('Gallery')
                  ],
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    IconButton(
                        onPressed: () async {
                          final result = await ImagePicker().pickImage(
                            imageQuality: 70,
                            maxWidth: 1440,
                            source: ImageSource.camera,
                          );

                          if (result != null) {
                            final bytes = await result.readAsBytes();
                            final image = await decodeImageFromList(bytes);

                            final message = types.ImageMessage(
                              author: _user,
                              createdAt: DateTime.now().millisecondsSinceEpoch,
                              height: image.height.toDouble(),
                              id: const Uuid().v4(),
                              name: result.name,
                              size: bytes.length,
                              uri: result.path,
                              width: image.width.toDouble(),
                            );

                            _addMessage(message);
                          }
                        },
                        icon: const Icon(
                          Icons.camera_alt,
                          size: 30,
                        )),
                    const Text('Camera')
                  ],
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    IconButton(
                        onPressed: () async {
                          final result = await FilePicker.platform.pickFiles(
                            type: FileType.any,
                          );

                          if (result != null &&
                              result.files.single.path != null) {
                            final message = types.FileMessage(
                              author: _user,
                              createdAt: DateTime.now().millisecondsSinceEpoch,
                              id: const Uuid().v4(),
                              mimeType:
                                  lookupMimeType(result.files.single.path!),
                              name: result.files.single.name,
                              size: result.files.single.size,
                              uri: result.files.single.path!,
                            );

                            _addMessage(message);
                          }
                        },
                        icon: const Icon(
                          Icons.insert_drive_file,
                          size: 30,
                        )),
                    const Text('File')
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleMessageTap(BuildContext _, types.Message message) async {
    if (message is types.FileMessage) {
      var localPath = message.uri;

      if (message.uri.startsWith('http')) {
        try {
          final index =
              _messages.indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (_messages[index] as types.FileMessage).copyWith(
            isLoading: true,
          );

          setState(() {
            _messages[index] = updatedMessage;
          });

          final client = http.Client();
          final request = await client.get(Uri.parse(message.uri));
          final bytes = request.bodyBytes;
          final documentsDir = (await getApplicationDocumentsDirectory()).path;
          localPath = '$documentsDir/${message.name}';

          if (!File(localPath).existsSync()) {
            final file = File(localPath);
            await file.writeAsBytes(bytes);
          }
        } finally {
          final index =
              _messages.indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (_messages[index] as types.FileMessage).copyWith(
            isLoading: null,
          );

          setState(() {
            _messages[index] = updatedMessage;
          });
        }
      }

      await OpenFilex.open(localPath);
    }
  }

  void _handleSendPressed(types.PartialText message) {
    final textMessage = types.TextMessage(
      author: _user,
      createdAt: DateTime.now().millisecond,
      id: const Uuid().v4(),
      text: message.text,
    );

    _addMessage(textMessage);
  }

  void _loadMessages() async {}

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: AppColors.LocationbarColor,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      widget.onSelectScreen(0);
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      size: 20,
                    )),
                text('Chat',
                    color: AppColors.commonblack,
                    weight: FontWeight.w400,
                    size: 14.sp),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.60,
                ),
                IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.call_outlined,
                      color: AppColors.chekboxActiveColor,
                    ))
              ],
            ),
            ListTile(
                leading: Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: ClipOval(
                    child: SizedBox(
                      width: 80.0,
                      height: 80.0,
                      child: Image.asset(
                        'assets/IMG.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                title: text('Savari',
                    color: AppColors.commonblack,
                    weight: FontWeight.w400,
                    size: 12.sp),
                subtitle: text('Customer',
                    color: AppColors.lightblack,
                    weight: FontWeight.w400,
                    size: 11.sp)),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.95,
              height: MediaQuery.of(context).size.height * 0.67,
              child: Chat(
                messages: _messages,
                onSendPressed: _handleSendPressed,
                user: _user,
                onAttachmentPressed: _handleAttachmentPressed,
                onMessageTap: _handleMessageTap,
                onMessageLongPress: (context, p1) {
                  const Dialog(
                    child: Text('Delete'),
                  );
                },
                theme: DefaultChatTheme(
                    inputTextCursorColor: AppColors.buttoncolor,
                    inputTextColor: Colors.black,
                    inputSurfaceTintColor: Colors.black,
                    attachmentButtonIcon: attachmentBlack,
                    sendButtonIcon: chatSend,
                    sendingIcon: chatSend,
                    primaryColor: Colors.white,
                    secondaryColor: Colors.white,
                    sentMessageDocumentIconColor: AppColors.buttoncolor,
                    sentMessageBodyTextStyle: GoogleFonts.notoSans(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                    sentMessageBodyLinkTextStyle: const TextStyle(
                      color: Colors.black,
                      decoration: TextDecoration.underline,
                    ),
                    sendButtonMargin: const EdgeInsets.all(6),
                    receivedMessageDocumentIconColor: AppColors.buttoncolor,
                    receivedMessageBodyTextStyle:
                        const TextStyle(color: Colors.white),
                    receivedMessageBodyLinkTextStyle: const TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                    inputPadding: const EdgeInsets.only(top: 0),
                    backgroundColor: AppColors.LocationbarColor,
                    inputBackgroundColor: Colors.white,
                    inputBorderRadius: BorderRadius.circular(10),
                    inputContainerDecoration: BoxDecoration(
                      border:
                          Border.all(color: AppColors.buttoncolor, width: 1),
                      borderRadius: BorderRadius.circular(10),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
