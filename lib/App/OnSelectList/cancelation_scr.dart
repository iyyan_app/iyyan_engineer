// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class Cancelation extends StatefulWidget {
  final Function(int) onSelectScreen;

  const Cancelation({super.key, required this.onSelectScreen});

  @override
  State<Cancelation> createState() => _CancelationState();
}

class _CancelationState extends State<Cancelation> {
  TextEditingController cancelrepController = TextEditingController();
  List<dynamic> cancelopt = [
    {'title': 'That location is very far away', 'isselect': false},
    {'title': 'Not interested this work', 'isselect': false},
    {'title': 'I don’t know this work', 'isselect': false},
    {'title': 'I ‘m not available now', 'isselect': false}
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.LocationbarColor,
      width: double.infinity,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 70.h,
              child: Column(
                children: [
                  SizedBox(
                    height: 4.h,
                  ),
                  text('Cancellation report',
                      color: AppColors.cancelBlack,
                      weight: FontWeight.w500,
                      size: 19.sp),
                  SizedBox(
                    height: 5.h,
                  ),
                  SizedBox(
                    width: 42.h,
                    height: MediaQuery.of(context).size.height * 0.24,
                    child: ListView.builder(
                      itemCount: cancelopt.length,
                      itemBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 5.h,
                          child: ListTile(
                            leading: GestureDetector(
                                ///////////////////////////importantent/////////////////
                                onTap: () {
                                  setState(() {
                                    for (var option in cancelopt) {
                                      option['isselect'] = false;
                                    }
                                    cancelopt[index]['isselect'] = true;
                                  });
                                },
                                child: cancelopt[index]['isselect']
                                    ? const Icon(
                                        Icons.check_circle,
                                        color: AppColors.chekboxActiveColor,
                                      )
                                    : const Icon(Icons.circle_outlined)),
                            title: text(cancelopt[index]['title'],
                                color: AppColors.commonblack,
                                weight: FontWeight.w400,
                                size: 12.sp),
                          ),
                        );
                      },
                    ),
                  ),
                  Container(
                    height: 18.h,
                    width: 45.h,
                    decoration: BoxDecoration(
                      color: AppColors.background_Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        TextField(
                          controller: cancelrepController,
                          keyboardType: TextInputType.name,
                          maxLines: null,
                          maxLengthEnforcement: MaxLengthEnforcement.enforced,
                          decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.only(left: 10, top: 3),
                              labelText: 'Reason',
                              labelStyle: GoogleFonts.notoSans(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400),
                              hintText: '',
                              hintStyle: GoogleFonts.notoSans(
                                  color: Colors.black45,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400),
                              border: const UnderlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                  borderSide: BorderSide.none)),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15.h,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 4.h,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.85,
              height: MediaQuery.of(context).size.height * 0.06,
              child: ElevatedButton(
                  style: ButtonStyle(
                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                      backgroundColor:
                          MaterialStateProperty.all(AppColors.ActiveTabcolor)),
                  onPressed: () {
                    setState(() {
                      widget.onSelectScreen(0);
                    });
                  },
                  child: text('Submit',
                      color: AppColors.background_Color,
                      weight: FontWeight.w400,
                      size: 16.sp)),
            )
          ],
        ),
      ),
    );
  }
}
