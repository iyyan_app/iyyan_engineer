import 'package:flutter/material.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import '../../CommenScreen/AppColors.dart';

class FirstOtp extends StatefulWidget {
  final Function onSelectScreen;
  const FirstOtp({super.key, required this.onSelectScreen});

  @override
  State<FirstOtp> createState() => _FirstOtpState();
}

class _FirstOtpState extends State<FirstOtp> with WidgetsBindingObserver {
  final firstNumber = TextEditingController();
  final secondNumber = TextEditingController();
  final thirdNumber = TextEditingController();
  final fourthNumber = TextEditingController();

  final firstFocus = FocusNode();
  final secondFocus = FocusNode();
  final thirdFocus = FocusNode();
  final fourthFocus = FocusNode();

  @override
  void dispose() {
    firstNumber.dispose();
    secondNumber.dispose();
    thirdNumber.dispose();
    fourthNumber.dispose();
    firstFocus.dispose();
    secondFocus.dispose();
    thirdFocus.dispose();
    fourthFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderScreen>(context, listen: false);
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 50.h,
                width: MediaQuery.of(context).size.width,
                color: AppColors.LocationbarColor,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      SizedBox(height: 4.h),
                      GestureDetector(
                        onTap: () {
                          widget.onSelectScreen(2);
                        },
                        child: Text(
                          'OTP Verification',
                          style: TextStyle(
                            color: AppColors.cancelBlack,
                            fontWeight: FontWeight.w500,
                            fontSize: 19.sp,
                          ),
                        ),
                      ),
                      SizedBox(height: 3.h),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          'Enter the 4 digits code that you received on your Phone number',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.lightblack,
                            fontWeight: FontWeight.w400,
                            fontSize: 12.sp,
                          ),
                        ),
                      ),
                      SizedBox(height: 3.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          myInputBox(
                              context, firstNumber, firstFocus, secondFocus),
                          myInputBox(
                              context, secondNumber, secondFocus, thirdFocus),
                          myInputBox(
                              context, thirdNumber, thirdFocus, fourthFocus),
                          myInputBox(context, fourthNumber, fourthFocus, null)
                        ],
                      ),
                      SizedBox(height: 5.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(Icons.access_time), //  clockIcon
                          const SizedBox(width: 10),
                          Consumer<ProviderScreen>(
                            builder: (context, prodValue1, child) {
                              return Text(
                                '00:${'${prodValue1.seconds}'.padLeft(2, '0')}',
                                style: TextStyle(
                                  color: AppColors.lightblack,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12.sp,
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 2.h),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                firstNumber.clear();
                                secondNumber.clear();
                                thirdNumber.clear();
                                fourthNumber.clear();
                                if (provider.isTimer == false) {
                                  provider.restartTimer1();
                                }
                              });
                            },
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.45,
                              height: 4.h,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(Icons.refresh), //  recentIcon
                                    const SizedBox(width: 10),
                                    Consumer<ProviderScreen>(
                                        builder: (context, otpmessage, child) {
                                      return Text(
                                          otpmessage.isTimer
                                              ? 'Enter the Code'
                                              : 'Resend the Code',
                                          style: TextStyle(
                                            color: AppColors.lightblack,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12.sp,
                                          ));
                                    }),
                                  ]),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 23.h),
              SizedBox(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: ElevatedButton(
                      onPressed: () {
                        widget.onSelectScreen(4);
                        provider.timer?.cancel();
                        provider.startAnalys();
                      },
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              AppColors.ActiveTabcolor),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)))),
                      child: Text(
                        'Continue',
                        style: TextStyle(
                          color: AppColors.background_Color,
                          fontWeight: FontWeight.w400,
                          fontSize: 18.sp,
                        ),
                      ))),
            ],
          ),
        ),
      ),
    );
  }
}

Widget myInputBox(BuildContext context, TextEditingController controller,
    FocusNode currentFocus, FocusNode? nextFocus) {
  return Container(
    height: 6.5.h,
    width: MediaQuery.of(context).size.width * 0.09,
    decoration: BoxDecoration(
      color: AppColors.background_Color,
      border: Border.all(width: 2, color: AppColors.buttoncolor),
      borderRadius: const BorderRadius.all(Radius.circular(15)),
    ),
    child: TextField(
      controller: controller,
      focusNode: currentFocus,
      maxLength: 1,
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      style: TextStyle(fontSize: 24.sp),
      decoration: const InputDecoration(
        counterText: '',
        contentPadding: EdgeInsets.all(0),
        border: OutlineInputBorder(borderSide: BorderSide.none),
      ),
      onChanged: (value) {
        if (value.length == 1) {
          if (nextFocus != null) {
            FocusScope.of(context).requestFocus(nextFocus);
          } else {
            currentFocus.unfocus();
          }
        }
      },
    ),
  );
}
