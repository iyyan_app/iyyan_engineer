import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:iyyan/App/dashboard.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:map_location_picker/map_location_picker.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class MapScreen extends StatefulWidget {
  final Function(int) onSelectScreen;

  const MapScreen({super.key, required this.onSelectScreen});

  @override
  State<MapScreen> createState() => _MapScreenState();
}

final List<Marker> _markers = [];
String latitude = '';
String longitude = '';
String name = '';
String street = '';
String locality = '';
String postal = '';
String countryy = '';

class _MapScreenState extends State<MapScreen> {
  final Completer<GoogleMapController> _controller = Completer();
  TextEditingController mylocationController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  static const CameraPosition _kGoogle = CameraPosition(
    target: LatLng(20.42796133580664, 80.885749655962),
    zoom: 4.4746,
  );

  LatLng? selectedLocation;
  Polyline? trackingPolyline;

  @override
  void initState() {
    super.initState();
    //   _getUserCurrentLocation();
  }

  Future<void> _getUserCurrentLocation() async {
    _markers.add(
      const Marker(
        markerId: MarkerId('1'),
        position: LatLng(20.42796133580664, 75.885749655962),
        infoWindow: InfoWindow(
          title: 'My Position',
        ),
      ),
    );
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Location services are disabled'),
        ),
      );
      return;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print('Location permissions are denied.');
        return;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      print(
          'Location permissions are permanently denied, we cannot request permissions.');
      return;
    }

    Position position = await Geolocator.getCurrentPosition();

    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    String address1 = placemarks.first.name ?? "";
    String address2 = placemarks.first.street ?? "";
    String address3 = placemarks.first.locality ?? "";
    String address4 = placemarks.first.postalCode ?? "";
    String address5 = placemarks.first.country ?? "";

    setState(() {
      _markers.add(
        Marker(
          markerId: const MarkerId('2'),
          position: LatLng(
            position.latitude,
            position.longitude,
          ),
          infoWindow: const InfoWindow(
            title: 'My Current Location',
          ),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        ),
      );
      latitude = position.latitude.toString();
      longitude = position.longitude.toString();
      name = address1.toString();
      street = address2.toString();
      locality = address3.toString();
      postal = address4.toString();
      countryy = address5.toString();
    });

    CameraPosition cameraPosition = CameraPosition(
      target: LatLng(
        position.latitude,
        position.longitude,
      ),
      zoom: 14,
    );

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  Future<void> selectNewLocation() async {
    final GoogleMapController controller = await _controller.future;
    LatLng latLng = await controller.getLatLng(const ScreenCoordinate(
        x: 100, y: 100)); // replace this with actual logic
    setState(() {
      selectedLocation = latLng;
      _markers.add(
        Marker(
          markerId: const MarkerId('3'),
          position: selectedLocation!,
          infoWindow: const InfoWindow(
            title: 'Selected Location',
          ),
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        ),
      );
    });
  }

  Future<void> _startTracking() async {
    if (selectedLocation != null) {
      final GoogleMapController controller = await _controller.future;
      LatLng currentLocation =
          LatLng(double.parse(latitude), double.parse(longitude));
      List<LatLng> route =
          await _getDrivingRoutePoints(currentLocation, selectedLocation!);
      setState(() {
        trackingPolyline = Polyline(
          polylineId: const PolylineId('tracking_line'),
          points: route,
          color: Colors.red,
          width: 5,
        );
      });
      for (LatLng point in route) {
        await Future.delayed(const Duration(seconds: 1));
        controller.animateCamera(CameraUpdate.newLatLng(point));
      }
    }
  }

  Future<List<LatLng>> _getDrivingRoutePoints(LatLng start, LatLng end) async {
    final String url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=${start.latitude},${start.longitude}&destination=${end.latitude},${end.longitude}&mode=driving&key=$apiKey';

    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final List<dynamic>? routes = data['routes'] as List<dynamic>?;

      if (routes != null && routes.isNotEmpty) {
        final List<dynamic>? legs = routes[0]['legs'] as List<dynamic>?;

        if (legs != null && legs.isNotEmpty) {
          final List<dynamic>? steps = legs[0]['steps'] as List<dynamic>?;

          if (steps != null && steps.isNotEmpty) {
            List<LatLng> route = [];
            for (var step in steps) {
              LatLng startLocation = LatLng(
                step['start_location']['lat'],
                step['start_location']['lng'],
              );
              LatLng endLocation = LatLng(
                step['end_location']['lat'],
                step['end_location']['lng'],
              );
              route.add(startLocation);
              route.add(endLocation);
            }
            return route;
          }
        }
      }
      throw Exception('No driving route points found in the response');
    } else {
      throw Exception('Failed to load driving directions');
    }
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderScreen>(context, listen: false);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 18.h,
          color: AppColors.LocationbarColor,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  widget.onSelectScreen(0);
                },
                child: const Padding(
                  padding: EdgeInsets.only(top: 3, left: 3),
                  child: Icon(
                    Icons.arrow_back,
                    size: 20,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Row(
                  children: [
                    locRatio,
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.06,
                      width: MediaQuery.of(context).size.width * 0.70,
                      child: TextField(
                        controller: mylocationController
                          ..text = '$locality$street',
                        decoration: InputDecoration(
                          hintText: 'Your location',
                          hintStyle: GoogleFonts.notoSans(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Colors.black87,
                          ),
                          labelText: 'Your location',
                          labelStyle: GoogleFonts.notoSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: AppColors.yourLocation,
                          ),
                          border: const UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black54),
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(12),
                              bottomRight: Radius.circular(12),
                            ),
                          ),
                          focusedBorder: const UnderlineInputBorder(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(12),
                              bottomRight: Radius.circular(12),
                            ),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          counterText: '',
                          contentPadding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 0),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.10,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Row(
                  children: [
                    location,
                    const SizedBox(
                      width: 20,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.06,
                      width: MediaQuery.of(context).size.width * 0.70,
                      child: TextField(
                        controller: locationController,
                        decoration: InputDecoration(
                          hintText: 'Choose location',
                          hintStyle: GoogleFonts.notoSans(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Colors.black87,
                          ),
                          labelText: 'Choose location',
                          labelStyle: GoogleFonts.notoSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: AppColors.commonblack,
                          ),
                          border: const UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black54),
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(12),
                              bottomRight: Radius.circular(12),
                            ),
                          ),
                          focusedBorder: const UnderlineInputBorder(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(12),
                              bottomRight: Radius.circular(12),
                            ),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          counterText: '',
                          contentPadding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 0),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.10,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Stack(children: [
          Container(
            height: 66.h,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey,
            child: GoogleMap(
              initialCameraPosition: _kGoogle,
              markers: Set<Marker>.of(_markers),
              mapType: MapType.normal,
              trafficEnabled: true,
              compassEnabled: true,
              indoorViewEnabled: true,
              buildingsEnabled: true,
              polylines: trackingPolyline != null ? {trackingPolyline!} : {},
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              onTap: (LatLng latLng) {
                setState(() {
                  selectedLocation = latLng;
                  _markers.add(
                    Marker(
                      markerId: const MarkerId('3'),
                      position: selectedLocation!,
                      infoWindow: const InfoWindow(
                        title: 'Selected Location',
                      ),
                      icon: BitmapDescriptor.defaultMarkerWithHue(
                          BitmapDescriptor.hueGreen),
                    ),
                  );
                });
              },
            ),
          ),
          Positioned(
            top: 300.0,
            right: 10.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    _getUserCurrentLocation();
                  },
                  child: Ink(
                    width: 50,
                    height: 50,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: const Icon(
                      Icons.my_location,
                      size: 30,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              height: 16.h,
              width: MediaQuery.of(context).size.width,
              color: AppColors.background_Color,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        text('1hr 30min',
                            color: AppColors.acceptbuttoncolor,
                            weight: FontWeight.w700,
                            size: 15.sp),
                        const SizedBox(
                          width: 6,
                        ),
                        text('(36 KM)',
                            color: AppColors.commonblack,
                            weight: FontWeight.w700,
                            size: 15.sp)
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          height: 8.h,
                          width: MediaQuery.of(context).size.width * 0.70,
                          child: text(
                              'Fastest route now due to traffic conditions',
                              color: AppColors.lightblack,
                              weight: FontWeight.w500,
                              size: 11.sp),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.04,
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(30),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                widget.onSelectScreen(3);
                                provider.startTimer1();
                                _startTracking();
                              },
                              child: Ink(
                                height: 5.h,
                                width: MediaQuery.of(context).size.width * 0.20,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.chekboxActiveColor),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      text('Start',
                                          color: AppColors.background_Color,
                                          weight: FontWeight.w400,
                                          size: 13.sp),
                                      locationTop,
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ]),
      ],
    );
  }
}
