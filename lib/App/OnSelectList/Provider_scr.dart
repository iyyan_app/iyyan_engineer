import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../CommenScreen/shared_preference.dart';
import '../../CommenScreen/url_all.dart';

class ProviderScreen extends ChangeNotifier {
  // FirstOtpScreen
  bool isTimer = false;
  int seconds = 60;
  Timer? timer;

  void startTimer1() {
    if (!isTimer) {
      timer = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (seconds == 0) {
          timer.cancel();
          isTimer = false;
        } else {
          seconds--;
        }
        notifyListeners();
      });
      isTimer = true;
      notifyListeners();
    }
  }

  void restartTimer1() {
    seconds = 60;
    isTimer = false;
    startTimer1();
  }

  // Ensure all timers are properly disposed of
  @override
  void dispose() {
    timer?.cancel();
    timer3?.cancel();
    timer5?.cancel();
    timer6?.cancel();
    timer2?.cancel();
    timer4?.cancel();
    timer7?.cancel();
    super.dispose();
  }

  // SecondOtpScreen
  bool isTimer3 = false;
  int seconds2 = 60;
  Timer? timer3;

  void startTimer2() {
    if (!isTimer3) {
      timer3 = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (seconds2 == 0) {
          timer.cancel();
          isTimer3 = false;
        } else {
          seconds2--;
        }
        notifyListeners();
      });
      isTimer3 = true;
      notifyListeners();
    }
  }

  void restartTimer2() {
    seconds2 = 60;
    isTimer3 = false;
    startTimer2();
  }

  // ThirdOtpScreen
  bool isTimer5 = false;
  int seconds3 = 60;
  Timer? timer5;

  void startTimer3() {
    if (!isTimer5) {
      timer5 = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (seconds3 == 0) {
          timer.cancel();
          isTimer5 = false;
        } else {
          seconds3--;
        }
        notifyListeners();
      });
      isTimer5 = true;
      notifyListeners();
    }
  }

  void restartTimer3() {
    seconds3 = 60;
    isTimer5 = false;
    startTimer3();
  }

  // FourthOtpScreen
  bool isTimer6 = false;
  int seconds4 = 60;
  Timer? timer6;

  void startTimer4() {
    if (!isTimer6) {
      timer6 = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (seconds4 == 0) {
          timer.cancel();
          isTimer6 = false;
        } else {
          seconds4--;
        }
        notifyListeners();
      });
      isTimer6 = true;
      notifyListeners();
    }
  }

  void restartTimer4() {
    seconds4 = 60;
    isTimer6 = false;
    startTimer4();
  }

  // AnalysisScreen
  Timer? timer2;
  bool isTimer2 = false;
  int analysisTime = 0;

  void startAnalys() {
    if (!isTimer2) {
      timer2 = Timer.periodic(const Duration(seconds: 1), (timer) {
        analysisTime++;
        notifyListeners();
      });
      isTimer2 = true;
      notifyListeners();
    }
  }

  void stopAnalys() {
    if (timer2 != null) {
      timer2!.cancel();
      isTimer2 = false;
      notifyListeners();
    }
  }

  String get timerString {
    int minutes = analysisTime ~/ 60;
    int seconds = analysisTime % 60;
    return '${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';
  }

  // Analysis2Screen
  Timer? timer4;
  bool isTimer4 = false;
  bool playpause = false;
  int analysisTime2 = 0;

  void startAnalys2() {
    if (!isTimer4 || timer4 == null || !timer4!.isActive) {
      timer4?.cancel();
      timer4 = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (!playpause) {
          analysisTime2++;
          notifyListeners();
        }
      });
      playpause = false;
      notifyListeners();
    }
  }

  void stopAnalys2() {
    if (timer4 != null) {
      timer4!.cancel();
      isTimer4 = false;
      notifyListeners();
    }
  }

  String get timerString2 {
    int minutes = analysisTime2 ~/ 60;
    int seconds = analysisTime2 % 60;
    return '${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';
  }

  // Analysis3Screen
  Timer? timer7;
  bool isTimer7 = false;
  bool playpause2 = false;
  int analysisTime3 = 0;

  void startAnalys3() {
    if (!isTimer7 || timer7 == null || !timer7!.isActive) {
      timer7?.cancel();
      timer7 = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (!playpause2) {
          analysisTime3++;
          notifyListeners();
        }
      });
      playpause2 = false;
      notifyListeners();
    }
  }

  void stopAnalys3() {
    if (timer7 != null) {
      timer7!.cancel();
      isTimer7 = false;
      notifyListeners();
    }
  }

  String get timerString3 {
    int minutes = analysisTime3 ~/ 60;
    int seconds = analysisTime3 % 60;
    return '${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';
  }

  // xxxxxxxxxxxxxxxxxxxxx  API INTEGRATIONS xxxxxxxxxxxxxxxxxxxxxxxxxx //
  String? myName;
  String? myEmail;
  String? myCenter;
  String? myCity;
  String? myNumber;
  String? myExperience;
  String? myWork;
  String? myTiming;
  String? myCost;

  Future<void> profileGet() async {
    try {
      notifyListeners();
      String? key = await AuthServices.getAuthKey();
      String? id = await AuthServices.getUserid();
      final response = await http
          .get(Uri.parse('$locationUrl$id'), headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'token $key',
      });
      if (response.statusCode == 200) {
        notifyListeners();
        final profileData = json.decode(response.body);
        final data1 = profileData['user_name'].toString();
        final data2 = profileData['email'].toString();
        final data3 = profileData['service_center'].toString();
        final data4 = profileData['locality'].toString();
        final data5 = profileData['contact_number'].toString();
        final data6 = profileData['experience'].toString();
        final data7 = profileData['specialized_in'].toString();
        final data8 = profileData['availability_time'].toString();
        final data9 = profileData['cost'].toString();
        myName = data1;
        myEmail = data2;
        myCenter = data3;
        myCity = data4;
        myNumber = data5;
        myExperience = data6;
        myWork = data7;
        myTiming = data8;
        myCost = data9;
      }
    } catch (e) {}
    notifyListeners();
  }
}
