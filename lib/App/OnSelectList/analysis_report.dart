import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class AnalysisReport extends StatefulWidget {
  final Function onSelectScreen;
  const AnalysisReport({super.key, required this.onSelectScreen});

  @override
  State<AnalysisReport> createState() => _AnalysisReportState();
}

bool showIssues = false;
bool showParts = false;

class _AnalysisReportState extends State<AnalysisReport>
    with TickerProviderStateMixin {
  AnimationController? controller;
  AnimationController? _controller;

  TextEditingController analyseReason = TextEditingController();
  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  void dispose() {
    controller!.dispose();
    _controller!.dispose();

    super.dispose();
  }

  List<dynamic> issuelist = [
    {'title': 'Slow Performance'},
    {'title': 'Overheating'},
    {'title': 'Battery Not Charging'},
    {'title': 'Screen Issues'},
    {'title': 'Hardware Failure'},
    {'title': 'Virus and Malware Infections'},
    {'title': 'Wi-Fi Connection Problems'},
    {'title': 'Hard Ddrive Failures'},
  ];
  List<dynamic> partslist = [
    {'title': 'Processor (CPU)'},
    {'title': 'Memory (RAM)'},
    {'title': 'Storage Drive'},
    {'title': 'Motherboard'},
    {'title': 'Graphics Processing Unit (GPU)'},
    {'title': 'Display Screen'},
    {'title': 'Keyboard and Touchpad'},
    {'title': 'Battery'},
    {'title': 'Cooling System'},
  ];
  String selectedissue = '';
  String selectedparts = '';

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderScreen>(context, listen: false);

    return Container(
      height: MediaQuery.of(context).size.height * 1,
      width: MediaQuery.of(context).size.width * 1,
      decoration: const BoxDecoration(color: AppColors.LocationbarColor),
      child: Padding(
        padding: const EdgeInsets.only(left: 40, right: 40),
        child: SingleChildScrollView(
          child: Stack(children: [
            Column(
              children: [
                SizedBox(
                  height: 4.h,
                ),
                GestureDetector(
                  onTap: () {
                    widget.onSelectScreen(4);
                  },
                  child: text('Analysis report',
                      color: AppColors.cancelBlack,
                      weight: FontWeight.w500,
                      size: 19.sp),
                ),
                SizedBox(
                  height: 5.h,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: text('Issues',
                      color: AppColors.lightblack,
                      weight: FontWeight.w500,
                      size: 10.sp),
                ),
                SizedBox(
                  height: 1.h,
                ),
                Container(
                  height: 6.h,
                  width: MediaQuery.of(context).size.width * 0.80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.background_Color),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 6.h,
                          width: MediaQuery.of(context).size.width * 0.68,
                          child: Center(
                            child: text(
                                selectedissue.isEmpty
                                    ? 'Issues'
                                    : selectedissue,
                                color: AppColors.cancelBlack,
                                weight: FontWeight.w400,
                                size: 12.sp),
                          ),
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                              onTap: () {
                                setState(() {
                                  showIssues = !showIssues;
                                  showParts = false;
                                  controller!.forward(from: 0.0);
                                });
                              },
                              child: RotationTransition(
                                turns: Tween(begin: 0.0, end: 0.5)
                                    .animate(controller!),
                                child: Icon(
                                  showIssues
                                      ? Icons.keyboard_arrow_down_rounded
                                      : Icons.keyboard_arrow_up_rounded,
                                  size: 28,
                                ),
                              )),
                        ),
                      ),
                      const SizedBox(
                        width: 1,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 4.h,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: text('Additional parts',
                      color: AppColors.lightblack,
                      weight: FontWeight.w500,
                      size: 10.sp),
                ),
                SizedBox(
                  height: 1.h,
                ),
                Container(
                  height: 6.h,
                  width: MediaQuery.of(context).size.width * 0.80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.background_Color),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 6.h,
                          width: MediaQuery.of(context).size.width * 0.68,
                          child: Center(
                            child: text(
                                selectedparts.isEmpty
                                    ? 'Additional Parts'
                                    : selectedparts,
                                color: AppColors.cancelBlack,
                                weight: FontWeight.w400,
                                size: 12.sp),
                          ),
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                showParts = !showParts;
                                showIssues = false;
                                _controller!.forward(from: 0.0);
                              });
                            },
                            child: RotationTransition(
                              turns: Tween(begin: 0.0, end: 0.5)
                                  .animate(_controller!),
                              child: Icon(
                                showParts
                                    ? Icons.keyboard_arrow_down_rounded
                                    : Icons.keyboard_arrow_up_rounded,
                                size: 28,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 1,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 8.h,
                ),
                Container(
                  height: 15.h,
                  width: 45.h,
                  decoration: BoxDecoration(
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextField(
                    controller: analyseReason,
                    keyboardType: TextInputType.name,
                    maxLines: null,
                    maxLengthEnforcement: MaxLengthEnforcement.enforced,
                    decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(
                          left: 10,
                        ),
                        labelText: 'Reason',
                        labelStyle: GoogleFonts.notoSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w400),
                        hintText: '',
                        hintStyle: GoogleFonts.notoSans(
                            color: Colors.black45,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                        border: const UnderlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: const UnderlineInputBorder(
                            borderSide: BorderSide.none)),
                  ),
                ),
                SizedBox(
                  height: 16.h,
                ),
                SizedBox(
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.75,
                    child: ElevatedButton(
                        onPressed: () {
                          widget.onSelectScreen(6);
                          provider.startTimer2();
                        },
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                                AppColors.ActiveTabcolor),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)))),
                        child: text('Submit',
                            color: AppColors.background_Color,
                            weight: FontWeight.w400,
                            size: 18.sp))),
                const SizedBox(
                  height: 20,
                )
              ],
            ),
            showIssues
                ? Positioned(
                    top: 166,
                    left: 40,
                    child: Container(
                      height: 40.h,
                      width: MediaQuery.of(context).size.width * 0.70,
                      decoration: BoxDecoration(
                          color: AppColors.background_Color,
                          borderRadius: BorderRadius.circular(8)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.builder(
                          itemCount: issuelist.length,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: 6.h,
                              child: RadioListTile(
                                  title: Text(issuelist[index]['title']),
                                  value: issuelist[index]['title'],
                                  groupValue: selectedissue,
                                  fillColor: MaterialStateColor.resolveWith(
                                      (states) => AppColors.ActiveTabcolor),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedissue = value?.toString() ?? '';
                                      Future.delayed(
                                          const Duration(milliseconds: 500),
                                          () {
                                        setState(() {
                                          showIssues = !showIssues;
                                        });
                                      });
                                    });
                                  }),
                            );
                          },
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
            showParts
                ? Positioned(
                    top: 264,
                    left: 40,
                    child: Container(
                      height: 40.h,
                      width: MediaQuery.of(context).size.width * 0.70,
                      decoration: BoxDecoration(
                          color: AppColors.background_Color,
                          borderRadius: BorderRadius.circular(8)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.builder(
                          itemCount: partslist.length,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return SingleChildScrollView(
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 6.h,
                                    child: RadioListTile(
                                        title: Text(partslist[index]['title']),
                                        value: partslist[index]['title'],
                                        groupValue: selectedparts,
                                        fillColor:
                                            MaterialStateColor.resolveWith(
                                                (states) =>
                                                    AppColors.ActiveTabcolor),
                                        onChanged: (value) {
                                          setState(() {
                                            selectedparts =
                                                value?.toString() ?? '';
                                            Future.delayed(
                                                const Duration(
                                                    milliseconds: 500), () {
                                              setState(() {
                                                showParts = !showParts;
                                              });
                                            });
                                          });
                                        }),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
          ]),
        ),
      ),
    );
  }
}
