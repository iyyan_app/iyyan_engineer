import 'package:flutter/material.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class QuatationScr extends StatefulWidget {
  final Function onSelectedScreen;
  const QuatationScr({super.key, required this.onSelectedScreen});

  @override
  State<QuatationScr> createState() => _QuatationScrState();
}

class _QuatationScrState extends State<QuatationScr> {
  List<Map<String, dynamic>> quatation = [
    {'title': 'Screen replacement', 'hours': '2', 'cost': '1000'},
    {'title': 'Labor Cost', 'hours': '2', 'cost': '1000'},
    {'title': 'Diagnostic fee', 'hours': '2', 'cost': '1000'},
    {'title': 'Additional parts cost', 'hours': '2', 'cost': '1000'},
    {'title': 'Taxes', 'hours': '2', 'cost': '1000'},
    {'title': 'Screen replacement', 'hours': '2', 'cost': '1000'},
    {'title': 'Labor Cost', 'hours': '2', 'cost': '1000'},
    {'title': 'Taxes', 'hours': '2', 'cost': '1000'},
    {'title': 'Screen replacement', 'hours': '2', 'cost': '1000'},
    {'title': 'Labor Cost', 'hours': '2', 'cost': '1000'},
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 1,
      width: MediaQuery.of(context).size.width * 1,
      color: AppColors.LocationbarColor,
      child: Column(
        children: [
          SizedBox(
            height: 15.h,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Icon(
                        Icons.check_circle,
                        color: AppColors.acceptbuttoncolor,
                      ),
                      const SizedBox(width: 3),
                      GestureDetector(
                        onTap: () {
                          widget.onSelectedScreen(11);
                        },
                        child: text('Completed',
                            color: AppColors.cancelBlack,
                            weight: FontWeight.w400,
                            size: 12.sp),
                      )
                    ],
                  ),
                  SizedBox(height: 1.h),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        text('Laptop Screen Repair',
                            color: AppColors.commonblack,
                            weight: FontWeight.w400,
                            size: 15.sp),
                        evviNew
                      ]),
                  SizedBox(height: 2.h),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: text('Quote Number: 231',
                        color: AppColors.buttoncolor,
                        weight: FontWeight.w500,
                        size: 10.sp),
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 1.h),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.58,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 9),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          Container(
                            height: 4.h,
                            width: MediaQuery.of(context).size.width,
                            color: AppColors.buttoncolor,
                            child: Row(children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.54,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: text('SERVICE DESCRIPTION',
                                      color: AppColors.background_Color,
                                      weight: FontWeight.w400,
                                      size: 10.sp),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.16,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: text('HOURS',
                                      color: AppColors.background_Color,
                                      weight: FontWeight.w400,
                                      size: 10.sp),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.25,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: text('COST',
                                        color: AppColors.background_Color,
                                        weight: FontWeight.w400,
                                        size: 10.sp),
                                  ),
                                ),
                              )
                            ]),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: quatation.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Column(
                                    children: [
                                      SizedBox(
                                        height: 5.h,
                                        child: Row(children: [
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.52,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: text(
                                                  '${quatation[index]['title']}',
                                                  color: AppColors.commonblack,
                                                  weight: FontWeight.w400,
                                                  size: 10.sp),
                                            ),
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.16,
                                            child: Align(
                                              alignment: Alignment.centerRight,
                                              child: text(
                                                  '${quatation[index]['hours']}',
                                                  color: AppColors.commonblack,
                                                  weight: FontWeight.w400,
                                                  size: 10.sp),
                                            ),
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.25,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 10),
                                              child: Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: text(
                                                    '${quatation[index]['cost']}',
                                                    color:
                                                        AppColors.commonblack,
                                                    weight: FontWeight.w400,
                                                    size: 10.sp),
                                              ),
                                            ),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 1,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.90,
                                        color: Colors.grey,
                                      )
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
                          Container(
                            height: 4.h,
                            width: MediaQuery.of(context).size.width,
                            color: AppColors.buttoncolor,
                            child: Row(children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.54,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: text('Total Cost',
                                      color: AppColors.background_Color,
                                      weight: FontWeight.w400,
                                      size: 10.sp),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.16,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: text('3',
                                      color: AppColors.background_Color,
                                      weight: FontWeight.w400,
                                      size: 10.sp),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.25,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: text('Rs.3399',
                                        color: AppColors.background_Color,
                                        weight: FontWeight.w400,
                                        size: 10.sp),
                                  ),
                                ),
                              )
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 2.h),
                  SizedBox(
                    //       height: 17.h,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Column(
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                text('Advanced Payment',
                                    color: AppColors.commonblack,
                                    weight: FontWeight.w400,
                                    size: 11.sp),
                                text('-Rs.1020',
                                    color: AppColors.commonblack,
                                    weight: FontWeight.w400,
                                    size: 11.sp)
                              ]),
                          SizedBox(height: 2.h),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                text('Remaining payment',
                                    color: AppColors.commonblack,
                                    weight: FontWeight.w400,
                                    size: 11.sp),
                                text('Rs.1020',
                                    color: AppColors.commonblack,
                                    weight: FontWeight.w400,
                                    size: 11.sp)
                              ]),
                          SizedBox(height: 2.h),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                text('Additional service cost',
                                    color: AppColors.commonblack,
                                    weight: FontWeight.w400,
                                    size: 11.sp),
                                text('Rs.1020',
                                    color: AppColors.commonblack,
                                    weight: FontWeight.w400,
                                    size: 11.sp)
                              ]),
                          SizedBox(height: 1.h),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 2.h),
                ],
              ),
            ),
          ),
          Container(
            height: 10.h,
            width: MediaQuery.of(context).size.width * 0.96,
            decoration: const BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 8.h,
                  width: MediaQuery.of(context).size.width * 0.40,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      text('Rs.3648',
                          color: AppColors.buttoncolor,
                          weight: FontWeight.w500,
                          size: 14.sp),
                      SizedBox(
                        height: 2.sp,
                      ),
                      text('Remaining',
                          color: AppColors.commonblack,
                          weight: FontWeight.w500,
                          size: 9.sp),
                    ],
                  ),
                ),
                SizedBox(
                    height: 6.h,
                    width: MediaQuery.of(context).size.width * 0.50,
                    child: ElevatedButton(
                        onPressed: () {
                          widget.onSelectedScreen(14);
                        },
                        style: ButtonStyle(
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          backgroundColor: MaterialStateProperty.all(
                            AppColors.chekboxActiveColor,
                          ),
                        ),
                        child: text('PAYMENT',
                            color: AppColors.background_Color,
                            weight: FontWeight.w500,
                            size: 14.sp)))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
