import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iyyan/App/Profile.dart';
import 'package:iyyan/App/Service.dart';
import 'package:iyyan/App/dashboard.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import '../Auth/SignupDetails.dart';
import '../CommenScreen/Allassets.dart';
import 'OnSelectList/Provider_scr.dart';
import 'OnSelectList/notification_scr.dart';

class HomeTabRoutes {
  static final Map<String, WidgetBuilder> routes = {
    '/dashboard': (context) => DashboardMenu(
          onSelectScreen: (int) {},
        ),
  };
}

class Dashboard extends StatefulWidget {
  const Dashboard({
    super.key,
  });

  @override
  State<Dashboard> createState() => _DashboardState();
}

int selectedPageIndex = 1;

class _DashboardState extends State<Dashboard> {
  // selectpage

  void selectPage(int index) {
    setState(() {
      selectedPageIndex = index;
    });
  }

  List<Widget> widgetOptions(BuildContext context) {
    return [
      Profile(onBack: () => selectPage(1)),
      ...HomeTabRoutes.routes.values
          .map((widgetBuilder) => widgetBuilder(context)),
      Service(onBack: () => selectPage(1)),
      NotificationScr(onBack: () => selectPage(1)),
    ];
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    final provider = Provider.of<ProviderScreen>(context, listen: false);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(8.h),
        child: Container(
          height: 20.h,
          width: MediaQuery.of(context).size.width,
          color: AppColors.background_Color,
          child: Row(
            children: [
              Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width * 0.35,
                alignment: Alignment.center,
                child: iyyanlogo,
              ),
              Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width * 0.45,
                alignment: Alignment.center,
                child: Container(
                  height: 4.h,
                  width: MediaQuery.of(context).size.width * 0.43,
                  decoration: const BoxDecoration(
                    color: AppColors.LocationbarColor,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.08,
                        height: double.infinity,
                        alignment: Alignment.center,
                        child: SizedBox(
                            width: 20, height: 20, child: LocationSearch),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.35,
                        height: double.infinity,
                        alignment: Alignment.centerLeft,
                        child: text(
                          locality2.isNotEmpty
                              ? locality2
                              : 'Where are You From',
                          color: Colors.black,
                          weight: FontWeight.w500,
                          size: 8.sp,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width * 0.2,
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                        onTap: () {
                          setState(() {
                            selectPage(3);
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(width: 25, height: 25, child: bell),
                        )),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: IndexedStack(
        index: selectedPageIndex,
        children: widgetOptions(context),
      ),
      bottomNavigationBar: Container(
        height: 8.h,
        width: MediaQuery.of(context).size.width,
        color: AppColors.buttoncolor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  selectPage(0);
                  provider.profileGet();
                });
              },
              child: Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width * 0.3,
                alignment: Alignment.topCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.25,
                  height: 6.h,
                  decoration: BoxDecoration(
                    color: selectedPageIndex == 0 ? Colors.white : null,
                    borderRadius: selectedPageIndex == 0
                        ? const BorderRadius.only(
                            bottomLeft: Radius.circular(60),
                            bottomRight: Radius.circular(60),
                          )
                        : null,
                  ),
                  child: Container(
                    height: 60,
                    width: 60,
                    alignment: Alignment.center,
                    child: selectedPageIndex == 0
                        ? activeprofile
                        : inactiveprofile,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  selectPage(1);
                });
              },
              child: Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width * 0.3,
                alignment: Alignment.topCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.25,
                  height: 6.h,
                  decoration: BoxDecoration(
                    color: selectedPageIndex == 1 || selectedPageIndex == 3
                        ? AppColors.LocationbarColor
                        : null,
                    borderRadius:
                        selectedPageIndex == 1 || selectedPageIndex == 3
                            ? const BorderRadius.only(
                                bottomLeft: Radius.circular(60),
                                bottomRight: Radius.circular(60),
                              )
                            : null,
                  ),
                  child: Container(
                    height: 60,
                    width: 60,
                    alignment: Alignment.center,
                    child: selectedPageIndex == 1 || selectedPageIndex == 3
                        ? activehome
                        : inactivehome,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  selectPage(2);
                });
              },
              child: Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width * 0.3,
                alignment: Alignment.topCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.25,
                  height: 6.h,
                  decoration: BoxDecoration(
                    color: selectedPageIndex == 2 ? Colors.white : null,
                    borderRadius: selectedPageIndex == 2
                        ? const BorderRadius.only(
                            bottomLeft: Radius.circular(60),
                            bottomRight: Radius.circular(60),
                          )
                        : null,
                  ),
                  child: Container(
                    height: 60,
                    width: 60,
                    alignment: Alignment.center,
                    child: selectedPageIndex == 2 ? activemic : inactivemic,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
