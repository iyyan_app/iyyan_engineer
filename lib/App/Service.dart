import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iyyan/CommenScreen/Allassets.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

class Service extends StatefulWidget {
  final VoidCallback onBack;

  const Service({super.key, required this.onBack});

  @override
  State<Service> createState() => _ServiceState();
}

class _ServiceState extends State<Service> {
  final List<Map<String, dynamic>> services = [
    {'id': '1', 'title': 'About', 'subtitle': 'About this App'},
    {
      'id': '2',
      'title': 'Contact Admin',
      'subtitle': 'Send message/ Queries / Feeddback to us'
    },
    {'id': '3', 'title': 'Call Admin', 'subtitle': 'Call us for your query'}
  ];
/*  int servicepagesIndex = 0;
  bool pagechange = false;
  void onitemtapped(int index) {
    setState(() {
      servicepagesIndex = index;
      pagechange = true;
    });
  }

  final List<Widget> widgetlist = <Widget>[
    const About(),
    const Admin(),
    const Calladmin()
  ];
*/

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(gradient: AppColors.linear1),
        child: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [oval1, oval2],
            ),
            Positioned(
                top: 1,
                child: IconButton(
                    onPressed: widget.onBack,
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: 22,
                    ))),
            Positioned(
              left: 10,
              right: 10,
              bottom: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.75,
                width: MediaQuery.of(context).size.width * 0.95,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6)),
                child: Column(
                  children: [
                    SizedBox(
                        height: 140,
                        width: MediaQuery.of(context).size.width * 0.95,
                        child: custcare),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.56,
                      width: MediaQuery.of(context).size.width,
                      child: ListView.builder(
                        itemCount: services.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Material(
                            child: InkWell(
                              autofocus: true,
                              onTap: () {
                                setState(() {
                                  if (index == 1) {
                                    showDialog(
                                        context: context,
                                        builder: (context) => const Admin());
                                  }
                                });
                              },
                              child: SizedBox(
                                height: 60,
                                child: ListTile(
                                    title: text('${services[index]['title']}',
                                        color: AppColors.commentlablecolor,
                                        weight: FontWeight.w400,
                                        size: 10.sp),
                                    subtitle: text(
                                        '${services[index]['subtitle']}',
                                        color: AppColors.servicepage,
                                        weight: FontWeight.w400,
                                        size: 9.sp),
                                    trailing: const Icon(
                                      Icons.arrow_forward_ios_rounded,
                                      size: 20,
                                      color: AppColors.chekboxActiveColor,
                                    )),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            /*  Positioned(
              bottom: 0,
              left: 10,
              right: 10,
              child: SizedBox(
                height: 100,
                width: double.infinity,
                child: widgetlist.elementAt(servicepagesIndex),
              ),
            )*/
          ],
        ));
  }
}

class About extends StatefulWidget {
  const About({super.key});

  @override
  State<About> createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber,
    );
  }
}

class Admin extends StatefulWidget {
  const Admin({super.key});

  @override
  State<Admin> createState() => _AdminState();
}

class _AdminState extends State<Admin> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.arrow_back_ios_new_outlined,
                      size: 20,
                    )),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.03,
                ),
                Text(
                  'Contact us',
                  style: GoogleFonts.notoSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                  ),
                )
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.70,
              height: 1,
              color: Colors.black26,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.70,
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.black)),
                child: TextField(
                  maxLines: null,
                  keyboardType: TextInputType.name,
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 10, top: 3),
                      prefixIcon: const Icon(Icons.mark_as_unread_sharp),
                      hintText: 'Enter your message here',
                      hintStyle: GoogleFonts.notoSans(
                          color: Colors.black38,
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                      border: const UnderlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                      focusedBorder: const UnderlineInputBorder(
                          borderSide: BorderSide.none)),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
              width: MediaQuery.of(context).size.width * 0.70,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(AppColors.ActiveTabcolor),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)))),
                  child: Text(
                    'SEND',
                    style: GoogleFonts.notoSans(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

class Calladmin extends StatefulWidget {
  const Calladmin({super.key});

  @override
  State<Calladmin> createState() => _CalladminState();
}

class _CalladminState extends State<Calladmin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
    );
  }
}
