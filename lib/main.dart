import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:iyyan/App/OnSelectList/Provider_scr.dart';
import 'package:iyyan/Auth/ContinueScreen.dart';
import 'package:iyyan/Auth/LoginScreen.dart';
import 'package:iyyan/Auth/MembershipScreen.dart';
import 'package:iyyan/Auth/ReturnLogin.dart';
import 'package:iyyan/Auth/SignupDetails.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:sizer/sizer.dart';

import 'Auth/SignupScreen.dart';

class Routes {
  static final Map<String, WidgetBuilder> routes = {
    '/': (context) => const LoginScreen(),
    '/signup': (context) => const SignupScreen(),
    '/signupDetails': (context) => const SignupDetails(),
    '/MembershipScreen': (context) => const MembershipScreen(),
    '/ContinueScreen': (context) => const ContinueScreen(),
    '/ReturnLogin': (context) => const ReturnLogin(),
  };
}

// class AppRoutes {
//   static final Map<String, WidgetBuilder> routes = {
//     '/Dashboard': (context) => const Dashboard(),
//   };
// }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  debugPaintSizeEnabled = false;
  runApp(IyyanEngineer());
}

class IyyanEngineer extends StatelessWidget {
  IyyanEngineer({super.key});

  final Map<String, WidgetBuilder> convertedRoutes =
      Routes.routes.map((key, value) {
    return MapEntry(key, (context) => value(context));
  });
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => ProviderScreen(),
          )
        ],
        child: MaterialApp(
          builder: (context, child) => ResponsiveBreakpoints.builder(
            child: child!,
            breakpoints: [
              const Breakpoint(start: 0, end: 450, name: MOBILE),
              const Breakpoint(start: 451, end: 800, name: TABLET),
              const Breakpoint(start: 801, end: 1920, name: DESKTOP),
              const Breakpoint(start: 1921, end: double.infinity, name: '4K'),
            ],
          ),
          /*  theme: ThemeData(
              textTheme: GoogleFonts.varelaTextTheme(Theme.of(context).textTheme)),*/
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: convertedRoutes,
        ),
      );
    });
  }
}
