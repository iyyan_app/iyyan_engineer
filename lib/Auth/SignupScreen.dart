import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iyyan/Auth/LoginScreen.dart';
import 'package:iyyan/Auth/SignupDetails.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

import '../CommenScreen/Allassets.dart';
import '../CommenScreen/AppColors.dart';
import '../CommenScreen/email_validator.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({super.key});

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

final nameController = TextEditingController();
final emailController = TextEditingController();
final mobileController = TextEditingController();

class _SignupScreenState extends State<SignupScreen> {
  final nameFocusNode = FocusNode();
  final emailFocusNode = FocusNode();
  final mobileFocusNode = FocusNode();
  final _formkey = GlobalKey<FormState>();
  bool isLoading = false;
  bool isChecked = false;

  void termsError() {
    var snackdemo = const SnackBar(
      content: Text('Please read Terms & Contitions'),
      behavior: SnackBarBehavior.floating,
      margin: EdgeInsets.all(15),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackdemo);
  }

  void ischecked() {
    setState(() {
      if (isChecked == true) {
        isChecked = isChecked;
      }
      if (isChecked == false) {
        isChecked = !isChecked;
      }
    });
  }

  void navigator() {
    Navigator.push(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) =>
              const SignupDetails(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = Offset(0.0, 0.0);
            const end = Offset.zero;
            const curve = Curves.ease;
            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
            var offsetAnimation = animation.drive(tween);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formkey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: AppColors.background_Color,
            child: SingleChildScrollView(
                child: Column(children: [
              Container(
                height: 41.3.h,
                width: double.infinity,
                alignment: Alignment.center,
                child: backgroundImage,
              ),
              Container(
                height: 10.h,
                width: double.infinity,
                alignment: Alignment.center,
                child: text('Create Account',
                    color: AppColors.loginAndsigin,
                    weight: FontWeight.w600,
                    size: 20.sp),
              ),
              SizedBox(
                height: 50.h,
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                      height: 10.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Name',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 9.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 6.h,
                              decoration: const BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: nameController,
                                  focusNode: nameFocusNode,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Name is Empty!';
                                    }
                                    if (value.length < 5) {
                                      return 'Enter valid Name';
                                    }
                                    return null;
                                  },
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'Name',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: Nameimg),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 8),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 10.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Mobile Number',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 9.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 6.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: mobileController,
                                  focusNode: mobileFocusNode,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Mobile Number is Empty!';
                                    }
                                    if (!isPhone(value)) {
                                      return 'Enter a valid Mobile Number';
                                    }
                                    return null;
                                  },
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'Mobile Number',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: bmobile),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 7),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 10.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('E-Mail',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 9.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 6.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: emailController,
                                  focusNode: emailFocusNode,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Email is Empty!';
                                    }
                                    if (!isEmail(value)) {
                                      return 'Please enter a valid Email';
                                    }
                                    return null;
                                  },
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'E-Mail',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: Email),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 8),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    SizedBox(
                      height: 4.h,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Checkbox(
                              shape: const RoundedRectangleBorder(
                                  side:
                                      BorderSide(width: 1, color: Colors.black),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              activeColor: AppColors.chekboxActiveColor,
                              value: isChecked,
                              onChanged: (val) {
                                setState(() {
                                  isChecked = val!;
                                });
                              }),
                          text('I agree the',
                              color: Colors.black,
                              weight: FontWeight.w400,
                              size: 11.sp),
                          TextButton(
                              onPressed: () {
                                showDialog(
                                    context: (context),
                                    builder: (context) => TermsCon(
                                          isChecked: false,
                                          onChecked: ischecked,
                                        ));
                              },
                              child: Container(
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 2, color: AppColors.Label))),
                                child: text('Privacy Policy',
                                    color: AppColors.Label,
                                    weight: FontWeight.w400,
                                    size: 11.sp),
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.28,
                      height: 3.5.h,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: AppColors.buttoncolor,
                            shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                            ),
                            side: const BorderSide(width: 0)),
                        onPressed: () {
                          setState(() {
                            _formkey.currentState!.validate();
                            isLoading = true;
                            if (isChecked == false) {
                              termsError();
                            }
                          });
                          Future.delayed(const Duration(seconds: 2), () {
                            setState(() {
                              isLoading = false;

                              if (isChecked) {
                                navigator();
                              }
                            });
                          });
                        },
                        child: isLoading
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 2.h,
                                    width: 2.h,
                                    child: const CircularProgressIndicator(
                                      color: Colors.white,
                                      strokeWidth: 2,
                                    ),
                                  ),
                                ],
                              )
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  text("SIGN UP",
                                      color: AppColors.buttontextColor,
                                      weight: FontWeight.w600,
                                      size: 8.sp),
                                  const SizedBox(
                                    width: 1,
                                  ),
                                  loginarrow
                                ],
                              ),
                      ),
                    ),
                    SizedBox(
                      height: 4.h,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          text('Already have a account?',
                              color: Colors.black,
                              weight: FontWeight.w400,
                              size: 10.sp),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: ((context) =>
                                          const LoginScreen())));
                            },
                            child: text('Login',
                                color: AppColors.signuptext,
                                weight: FontWeight.w500,
                                size: 10.sp),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ]))),
      ),
    );
  }
}

class TermsCon extends StatefulWidget {
  const TermsCon({super.key, required this.isChecked, required this.onChecked});
  final bool isChecked;
  final VoidCallback onChecked;
  @override
  State<TermsCon> createState() => _TermsConState();
}

class _TermsConState extends State<TermsCon> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                'Terms And Conditions',
                style: GoogleFonts.notoSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: AppColors.buttoncolor),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
                style: GoogleFonts.notoSans(
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                    onPressed: () {
                      setState(() {
                        widget.onChecked();
                        Navigator.pop(context);
                      });
                    },
                    child: const Text('Ok')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
