// import 'package:chachi/components/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:iyyan/Auth/LoginScreen.dart';
import 'package:iyyan/Auth/SignupScreen.dart';

class Routes {
  static final Map<String, WidgetBuilder> routes = {
    '/': (context) => const LoginScreen(), // Default route
    '/signup': (context) => const SignupScreen(),
    // Add more routes as needed
  };
  Map<String, WidgetBuilder>? convertedRoutes = routes.map((key, value) {
    return MapEntry(key, (context) => value(context));
  });
}
