import 'package:flutter/material.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

import '../CommenScreen/Allassets.dart';
import '../CommenScreen/AppColors.dart';

class ContinueScreen extends StatefulWidget {
  const ContinueScreen({super.key});

  @override
  State<ContinueScreen> createState() => _ContinueScreenState();
}

class _ContinueScreenState extends State<ContinueScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: AppColors.background_Color,
          child: SingleChildScrollView(
              child: Column(children: [
            Container(
              height: 55.h,
              alignment: Alignment.bottomCenter,
              child: cont,
            ),
            Container(
              height: 15.h,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  text('After Cancel three',
                      color: AppColors.commenTextColor,
                      weight: FontWeight.w300,
                      size: 20.sp),
                  text('tickets you will',
                      color: AppColors.commenTextColor,
                      weight: FontWeight.w300,
                      size: 20.sp),
                  text('perform',
                      color: AppColors.commenTextColor,
                      weight: FontWeight.w300,
                      size: 20.sp),
                ],
              ),
            ),
            Container(
              height: 5.h,
            ),
            Container(
              height: 10.h,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  text('one ticket without any',
                      color: AppColors.chekboxActiveColor,
                      weight: FontWeight.w300,
                      size: 20.sp),
                  text('cost from your side',
                      color: AppColors.chekboxActiveColor,
                      weight: FontWeight.w300,
                      size: 20.sp),
                ],
              ),
            ),
            Container(
              height: 5.h,
            ),
            Container(
              height: 5.h,
              alignment: Alignment.center,
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, '/ReturnLogin');
                },
                child: Container(
                  width: MediaQuery.of(context).size.width *
                      0.27, // Adjust the width as needed
                  height: 3.7.h,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(85, 123, 198, 1),
                          offset: const Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                      color: AppColors.buttoncolor,
                      borderRadius: BorderRadius.all(Radius.circular(20))),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      text("CONTINUE",
                          color: AppColors.buttontextColor,
                          weight: FontWeight.w600,
                          size: 8.sp),
                      SizedBox(
                        width: 10,
                      ),
                      loginarrow
                    ],
                  ),
                ),
              ),
            ),
          ]))),
    );
  }
}
