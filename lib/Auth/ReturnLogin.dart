import 'package:flutter/material.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:sizer/sizer.dart';

import '../CommenScreen/Allassets.dart';
import '../CommenScreen/AppColors.dart';

class ReturnLogin extends StatefulWidget {
  const ReturnLogin({super.key});

  @override
  State<ReturnLogin> createState() => _ReturnLoginState();
}

class _ReturnLoginState extends State<ReturnLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: AppColors.background_Color,
          child: SingleChildScrollView(
              child: Column(children: [
            Container(
              height: 50.h,
              width: double.infinity,
              alignment: Alignment.bottomCenter,
              child: ret,
            ),
            Container(
              height: 15.h,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  text('You will login, After our admin Verify and',
                      color: AppColors.commenTextColor,
                      weight: FontWeight.w500,
                      size: 12.sp),
                  text('Approve your profile.',
                      color: AppColors.commenTextColor,
                      weight: FontWeight.w500,
                      size: 12.sp)
                ],
              ),
            ),
            Container(
              height: 25.h,
              width: double.infinity,
            ),
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/');
              },
              child: Container(
                height: 5.h,
                width: double.infinity,
                alignment: Alignment.center,
                child: Container(
                  width: MediaQuery.of(context).size.width *
                      0.27, // Adjust the width as needed
                  height: 3.7.h,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(85, 123, 198, 1),
                          offset: const Offset(1, 3),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                      color: AppColors.buttoncolor,
                      borderRadius: BorderRadius.all(Radius.circular(20))),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      text("LOGIN",
                          color: AppColors.buttontextColor,
                          weight: FontWeight.w600,
                          size: 8.sp),
                      SizedBox(
                        width: 10,
                      ),
                      loginarrow
                    ],
                  ),
                ),
              ),
            )
          ]))),
    );
  }
}
