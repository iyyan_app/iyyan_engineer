import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:iyyan/Auth/MembershipScreen.dart';
import 'package:iyyan/Auth/SignupScreen.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:map_location_picker/map_location_picker.dart';
import 'package:sizer/sizer.dart';

import '../CommenScreen/Allassets.dart';
import '../CommenScreen/AppColors.dart';
import '../CommenScreen/url_all.dart';

class SignupDetails extends StatefulWidget {
  const SignupDetails({
    super.key,
  });

  @override
  State<SignupDetails> createState() => _SignupDetailsState();
}

class _SignupDetailsState extends State<SignupDetails> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final servicecenterController = TextEditingController();
  final mobilenuController = TextEditingController();
  final specializedController = TextEditingController();
  final hourcostController = TextEditingController();
  final hourcostController2 = TextEditingController();

  final firstnameFocus = FocusNode();
  final lastnameFocus = FocusNode();
  final serviceFocus = FocusNode();
  final mobileFocus = FocusNode();
  final specializedFocus = FocusNode();
  final hourcostFocus = FocusNode();
  final hourcostFocus2 = FocusNode();
  DateTime? fromDateTime;
  DateTime? toDateTime;

  @override
  void dispose() {
    hourcostFocus.dispose();
    hourcostFocus2.dispose();
    super.dispose();
  }

  String experienceYear = '1 - Years';
  var items = List.generate(6, (index) => '${index + 1} - Years');

  /* String amount = '100 - 200';
var items = List.generate(5, (index) => '${(index + 3) * 100} - ${(index + 4) * 100}');
 */

  Future<void> signupPost() async {
    try {
      final response = await http.post(Uri.parse(signupUrl),
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode({
            'user_name': nameController.text,
            'email': emailController.text,
            'contact_number': mobileController.text,
            'first_name': firstnameController.text,
            'last_name': lastnameController.text,
            'locality': locality2,
            'experience': experienceYear,
            'specialized_in': specializedController.text,
            'availability_time': getTimeText(),
            'cost': '${hourcostController.text} - ${hourcostController2.text} ',
            'role': '6628bcabbd9c0804497cfb11',
          }));
      if (response.statusCode == 201) {
        setState(() {
          navigator();
          print('signup success');
        });
      }
    } catch (e) {
      print('error:$e');
    }
  }

  void navigator() {
    Navigator.push(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) =>
              const MembershipScreen(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = Offset(0.0, 0.0);
            const end = Offset.zero;
            const curve = Curves.ease;
            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
            var offsetAnimation = animation.drive(tween);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
        ));
  }

  void timepick() {
    showDialog(
        context: context,
        builder: (context) {
          return Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.85,
              height: MediaQuery.of(context).size.height * 0.20,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_back_ios_new,
                            size: 20,
                          )),
                      const SizedBox(
                        width: 55,
                      ),
                      Text(
                        'Select Time',
                        style: GoogleFonts.notoSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.black,
                            decoration: TextDecoration.none),
                      ),
                    ],
                  ),
                  Container(
                    height: 1,
                    width: MediaQuery.of(context).size.width * 0.80,
                    color: Colors.black,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 55,
                        child: Row(
                          children: [
                            Text(
                              'From',
                              style: GoogleFonts.notoSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black,
                                  decoration: TextDecoration.none),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            TimePickerSpinner(
                              is24HourMode: false,
                              isShowSeconds: false,
                              normalTextStyle: const TextStyle(
                                  fontSize: 0,
                                  color: Colors.transparent,
                                  decoration: TextDecoration.none),
                              highlightedTextStyle: const TextStyle(
                                  fontSize: 20,
                                  color: Colors.black54,
                                  decoration: TextDecoration.none),
                              spacing: 5,
                              itemHeight: 25,
                              itemWidth: 25,
                              isForce2Digits: true,
                              onTimeChange: (time) {
                                setState(() {
                                  fromDateTime = time;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 55,
                        child: Row(
                          children: [
                            Text(
                              'To',
                              style: GoogleFonts.notoSans(
                                  fontSize: 1,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black,
                                  decoration: TextDecoration.none),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            TimePickerSpinner(
                              is24HourMode: false,
                              normalTextStyle: const TextStyle(
                                  fontSize: 0,
                                  color: Colors.transparent,
                                  decoration: TextDecoration.none),
                              highlightedTextStyle: const TextStyle(
                                  fontSize: 20,
                                  color: Colors.black54,
                                  decoration: TextDecoration.none),
                              spacing: 5,
                              itemHeight: 25,
                              itemWidth: 25,
                              isForce2Digits: true,
                              onTimeChange: (time) {
                                setState(() {
                                  toDateTime = time;
                                });
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 200),
                    child: SizedBox(
                      height: 25,
                      width: 90,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              AppColors.acceptbuttoncolor),
                          overlayColor: MaterialStateProperty.all(
                              AppColors.acceptbuttoncolor),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                        ),
                        child: Text(
                          'SAVE',
                          style: GoogleFonts.notoSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  String getTimeText() {
    if (fromDateTime != null && toDateTime != null) {
      return '${(fromDateTime!.hour > 12) ? fromDateTime!.hour - 12 : fromDateTime!.hour} ${(fromDateTime!.hour >= 12) ? 'PM' : 'AM'} - ${(toDateTime!.hour > 12) ? toDateTime!.hour - 12 : toDateTime!.hour} ${(toDateTime!.hour >= 12) ? 'PM' : 'AM'}';
    } else {
      return 'Time';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formkey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: AppColors.background_Color,
            child: SingleChildScrollView(
                child: Column(children: [
              SizedBox(
                height: 16.h,
                width: double.infinity,
                child: Container(
                  height: 16.h,
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: detailimg,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 4.h,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  child: text('Add Details',
                      color: AppColors.loginAndsigin,
                      weight: FontWeight.w600,
                      size: 25.sp),
                ),
              ),
              SizedBox(
                height: 80.h,
                child: Column(
                  children: [
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('First Name',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: firstnameController,
                                  focusNode: firstnameFocus,
                                  maxLength: 20,
                                  validator: (value) => value!.isEmpty
                                      ? 'First name is Empty'
                                      : null,
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    counterText: '',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: Nameimg),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none,
                                    ),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 7),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Last Name',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: lastnameController,
                                  focusNode: lastnameFocus,
                                  maxLength: 20,
                                  validator: (value) => value!.isEmpty
                                      ? 'Last name number is Empty'
                                      : null,
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    counterText: '',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: Nameimg),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none,
                                    ),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 7),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Service center name',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: servicecenterController,
                                  focusNode: serviceFocus,
                                  maxLength: 20,
                                  validator: (value) => value!.isEmpty
                                      ? 'Service center name is Empty'
                                      : null,
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    counterText: '',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: service),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none,
                                    ),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 7),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Location',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 17, right: 16),
                                        child: blocation,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.60,
                                        child: Text(
                                          name2.isEmpty
                                              ? 'Select your Location'
                                              : locality2,
                                          style: GoogleFonts.notoSans(
                                            fontSize: 10.sp,
                                            fontWeight: FontWeight.w500,
                                            color: const Color.fromRGBO(
                                                34, 16, 0, 1),
                                          ),
                                        ),
                                      ),
                                      IconButton(
                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder: (context) =>
                                                    const LocationPick());
                                          },
                                          icon: const Icon(
                                            Icons.my_location_sharp,
                                            size: 22,
                                            color: Colors.black54,
                                          )),
                                    ]),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Experience',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 17, right: 16),
                                        child: experiance,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.69,
                                        child: DropdownButton(
                                          isExpanded: true,
                                          underline: const SizedBox(),
                                          style: GoogleFonts.notoSans(
                                            fontSize: 10.sp,
                                            fontWeight: FontWeight.w500,
                                            color: const Color.fromRGBO(
                                                34, 16, 0, 1),
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          value: experienceYear,
                                          icon: const Icon(
                                            Icons.keyboard_arrow_down_rounded,
                                            size: 22,
                                            color: Colors.black54,
                                          ),
                                          items: items.map((String items) {
                                            return DropdownMenuItem(
                                                value: items,
                                                child: Text(
                                                  items,
                                                ));
                                          }).toList(),
                                          onChanged: (String? newvalue) {
                                            setState(() {
                                              experienceYear = newvalue!;
                                            });
                                          },
                                        ),
                                      ),
                                    ]),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Specialized In',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: TextFormField(
                                  controller: specializedController,
                                  focusNode: specializedFocus,
                                  maxLength: 25,
                                  validator: (value) => value!.isEmpty
                                      ? 'Specialized In is Empty'
                                      : null,
                                  cursorColor: AppColors.buttoncolor,
                                  style: GoogleFonts.notoSans(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(34, 16, 0, 1),
                                  ),
                                  decoration: InputDecoration(
                                    counterText: '',
                                    prefixIcon: Container(
                                        height: 50,
                                        width: 50,
                                        alignment: Alignment.center,
                                        child: speciallist),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none,
                                    ),
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 15.0, vertical: 7),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Availability Time',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 17, right: 16),
                                        child: available,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.60,
                                        child: Text(
                                          getTimeText(),
                                          style: GoogleFonts.notoSans(
                                            fontSize: 10.sp,
                                            fontWeight: FontWeight.w500,
                                            color: const Color.fromRGBO(
                                                34, 16, 0, 1),
                                          ),
                                        ),
                                      ),
                                      IconButton(
                                          onPressed: () {
                                            timepick();
                                          },
                                          icon: const Icon(
                                            Icons.access_time_sharp,
                                            size: 22,
                                            color: Colors.black54,
                                          ))
                                    ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 9.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: double.infinity,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 3.h,
                              width: double.infinity,
                              alignment: Alignment.centerLeft,
                              child: text('Cost per hour / Service',
                                  color: AppColors.Label,
                                  weight: FontWeight.w500,
                                  size: 10.sp),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 5.h,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 5,
                                shadowColor: Colors.black,
                                color: Colors.white,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 17, right: 16),
                                        child: cost,
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.60,
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.12,
                                              child: TextFormField(
                                                controller: hourcostController,
                                                focusNode: hourcostFocus,
                                                keyboardType:
                                                    TextInputType.number,
                                                onEditingComplete: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          hourcostFocus2);
                                                },
                                                cursorColor:
                                                    AppColors.buttoncolor,
                                                style: GoogleFonts.notoSans(
                                                  fontSize: 10.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: const Color.fromRGBO(
                                                      34, 16, 0, 1),
                                                ),
                                                maxLength: 4,
                                                decoration: InputDecoration(
                                                  hintText: '100',
                                                  counterText: '',
                                                  errorBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  border:
                                                      const OutlineInputBorder(
                                                          borderSide:
                                                              BorderSide.none),
                                                  contentPadding:
                                                      const EdgeInsets
                                                          .symmetric(
                                                          horizontal: 1.0,
                                                          vertical: 7),
                                                ),
                                              ),
                                            ),
                                            const Text(
                                              '-',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.20,
                                              child: TextFormField(
                                                controller: hourcostController2,
                                                focusNode: hourcostFocus2,
                                                maxLength: 4,
                                                keyboardType:
                                                    TextInputType.number,
                                                cursorColor:
                                                    AppColors.buttoncolor,
                                                style: GoogleFonts.notoSans(
                                                  fontSize: 10.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: const Color.fromRGBO(
                                                      34, 16, 0, 1),
                                                ),
                                                decoration: InputDecoration(
                                                  counterText: '',
                                                  hintText: '200',
                                                  errorBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  border:
                                                      const OutlineInputBorder(
                                                          borderSide:
                                                              BorderSide.none),
                                                  contentPadding:
                                                      const EdgeInsets
                                                          .symmetric(
                                                          horizontal: 12.0,
                                                          vertical: 7),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      IconButton(
                                          onPressed: () {
                                            FocusScope.of(context)
                                                .requestFocus(hourcostFocus);
                                          },
                                          icon: const Icon(
                                            Icons.keyboard_alt_outlined,
                                            color: Colors.black54,
                                            size: 22,
                                          ))

                                      /*  TextButton(
                                        onPressed: () {
                                          timepick();
                                        },
                                        child: const Text('\u{20B9}'),
                                      )*/
                                    ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 8.h,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            _formkey.currentState!.validate();
                            signupPost();
                          });
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.25,
                          height: 3.5.h,
                          decoration: const BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(85, 123, 198, 1),
                                  offset: Offset(1, 3),
                                  blurRadius: 5,
                                  spreadRadius: 1,
                                )
                              ],
                              color: AppColors.buttoncolor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          alignment: Alignment.center,
                          child: text("Complete",
                              color: AppColors.buttontextColor,
                              weight: FontWeight.w600,
                              size: 8.sp),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ]))),
      ),
    );
  }
}

class LocationPick extends StatefulWidget {
  const LocationPick({super.key});

  @override
  State<LocationPick> createState() => _LocationPickState();
}

final List<Marker> _markers = [];
String latitude2 = '';
String longitude2 = '';
String name2 = '';
String street2 = '';
String locality2 = '';
String postal2 = '';
String countryy2 = '';

class _LocationPickState extends State<LocationPick> {
  final Completer<GoogleMapController> _controller = Completer();

  static const CameraPosition _kGoogle = CameraPosition(
    target: LatLng(20.42796133580664, 80.885749655962),
    zoom: 4.4746,
  );

  LatLng? selectedLocation;
  Polyline? trackingPolyline;

  Future<void> _getUserCurrentLocation() async {
    _markers.add(
      const Marker(
        markerId: MarkerId('1'),
        position: LatLng(20.42796133580664, 75.885749655962),
        infoWindow: InfoWindow(
          title: 'My Position',
        ),
      ),
    );
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Location services are disabled'),
        ),
      );
      return;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print('Location permissions are denied.');
        return;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      print(
          'Location permissions are permanently denied, we cannot request permissions.');
      return;
    }

    Position position = await Geolocator.getCurrentPosition();

    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    String address1 = placemarks.first.name ?? "";
    String address2 = placemarks.first.street ?? "";
    String address3 = placemarks.first.locality ?? "";
    String address4 = placemarks.first.postalCode ?? "";
    String address5 = placemarks.first.country ?? "";

    setState(() {
      _markers.add(
        Marker(
          markerId: const MarkerId('2'),
          position: LatLng(
            position.latitude,
            position.longitude,
          ),
          infoWindow: const InfoWindow(
            title: 'My Current Location',
          ),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        ),
      );
      latitude2 = position.latitude.toString();
      longitude2 = position.longitude.toString();
      name2 = address1.toString();
      street2 = address2.toString();
      locality2 = address3.toString();
      postal2 = address4.toString();
      countryy2 = address5.toString();
    });

    CameraPosition cameraPosition = CameraPosition(
      target: LatLng(
        position.latitude,
        position.longitude,
      ),
      zoom: 14,
    );

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        child: Stack(children: [
      ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: SizedBox(
          height: 66.h,
          width: MediaQuery.of(context).size.width,
          child: GoogleMap(
            initialCameraPosition: _kGoogle,
            markers: Set<Marker>.of(_markers),
            mapType: MapType.normal,
            trafficEnabled: true,
            compassEnabled: true,
            indoorViewEnabled: true,
            buildingsEnabled: true,
            polylines: trackingPolyline != null ? {trackingPolyline!} : {},
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
            onTap: (LatLng latLng) {
              setState(() {
                selectedLocation = latLng;
                _markers.add(
                  Marker(
                    markerId: const MarkerId('3'),
                    position: selectedLocation!,
                    infoWindow: const InfoWindow(
                      title: 'Selected Location',
                    ),
                    icon: BitmapDescriptor.defaultMarkerWithHue(
                        BitmapDescriptor.hueGreen),
                  ),
                );
              });
            },
          ),
        ),
      ),
      Positioned(
        top: 300.0,
        right: 10.0,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                _getUserCurrentLocation();
              },
              child: Ink(
                width: 50,
                height: 50,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: const Icon(
                  Icons.my_location,
                  size: 30,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ),
      ),
      Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        child: Container(
          height: 13.h,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10)),
            color: AppColors.background_Color,
          ),
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Column(
              children: [
                SizedBox(
                  height: 1.h,
                ),
                SizedBox(
                  height: 8.h,
                  width: MediaQuery.of(context).size.width * 0.70,
                  child: text(
                      name2.isEmpty
                          ? 'Select the location'
                          : '$name2, $street2, $locality2, $postal2, $countryy2',
                      color: AppColors.commentlablecolor,
                      weight: FontWeight.w500,
                      size: 12.sp),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 3.h,
                          decoration: BoxDecoration(
                              color: AppColors.acceptbuttoncolor,
                              borderRadius: BorderRadius.circular(8)),
                          width: MediaQuery.of(context).size.width * 0.20,
                          child: Center(
                            child: text('DONE',
                                color: AppColors.background_Color,
                                weight: FontWeight.w600,
                                size: 9.sp),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    ]));
  }
}
