import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:iyyan/App/appBar_bottomBar.dart';
import 'package:iyyan/CommenScreen/AppColors.dart';
import 'package:iyyan/CommenScreen/CommenWidget.dart';
import 'package:iyyan/CommenScreen/shared_preference.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';

import '../CommenScreen/Allassets.dart';
import '../CommenScreen/email_validator.dart';
import '../CommenScreen/url_all.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    super.key,
  });

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final mobileController = TextEditingController();
  final passwordController = TextEditingController();
  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();
  final _formkey = GlobalKey<FormState>();

  bool passwordvisible = false;
  bool isLoading = false;

  Future<void> loginPost() async {
    try {
      String gmailregExp = r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$';
      String contactNumber = mobileController.text;
      String email = '';

      if (RegExp(gmailregExp).hasMatch(contactNumber)) {
        email = contactNumber;
        contactNumber = '';
      }
      final response = await http.post(Uri.parse(loginUrl),
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode({
            'contact_number': contactNumber,
            'email': email,
            'password': passwordController.text,
          }));
      if (response.statusCode == 200) {
        final jsonResponse = json.decode(response.body);
        final id = jsonResponse['data']['user_data']['id'];
        final authkey = jsonResponse['data']['auth_key'];
        await AuthServices.saveUserid(id);
        await AuthServices.saveAuthkey(authkey);

        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('authkey', authkey);
        await prefs.setString('id', id);
        navigator();
      }
    } catch (e) {
      print('Error:$e');
    }
  }

  void navigator() {
    Navigator.push(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) =>
              const Dashboard(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = Offset(0.0, 0.0);
            const end = Offset.zero;
            const curve = Curves.ease;
            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
            var offsetAnimation = animation.drive(tween);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return Scaffold(
      body: Form(
        key: _formkey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: AppColors.background_Color,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 41.3.h,
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: backgroundImage,
                ),
                SizedBox(
                  height: 10.h,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Container(
                        height: 5.h,
                        width: double.infinity,
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 8.w),
                        child: text('Login',
                            color: AppColors.loginAndsigin,
                            weight: FontWeight.w600,
                            size: 20.sp),
                      ),
                      Container(
                        height: 5.h,
                        width: double.infinity,
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 8.w),
                        child: text('sign in to continuous',
                            color: AppColors.loginAndsigin,
                            weight: FontWeight.w500,
                            size: 12.sp),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 6.h,
                ),
                SizedBox(
                  height: 35.h,
                  width: double.infinity,
                  child: Column(
                    children: [
                      Container(
                        height: 10.h,
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: SizedBox(
                          height: double.infinity,
                          width: MediaQuery.of(context).size.width * 0.85,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: 3.h,
                                width: double.infinity,
                                alignment: Alignment.centerLeft,
                                child: text('E-Mail / Mobile Number',
                                    color: AppColors.Label,
                                    weight: FontWeight.w500,
                                    size: 10.sp),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.90,
                                height: 6.h,
                                decoration: const BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Material(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  elevation: 5,
                                  shadowColor: Colors.black,
                                  color: Colors.white,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: TextFormField(
                                      controller: mobileController,
                                      focusNode: emailFocusNode,
                                      cursorColor: AppColors.buttoncolor,
                                      style: GoogleFonts.notoSans(
                                        fontSize: 9.sp,
                                        fontWeight: FontWeight.w500,
                                        color:
                                            const Color.fromRGBO(34, 16, 0, 1),
                                      ),
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return 'Email / Mobile Number is Empty';
                                        }
                                        if (!isEmail(value) &&
                                            !isPhone(value)) {
                                          return 'Please enter a valid Email';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'E-Mail / Mobile Number',
                                        prefixIcon: Container(
                                            height: 50,
                                            width: 20,
                                            alignment: Alignment.center,
                                            child: Email),
                                        border: const OutlineInputBorder(
                                            borderSide: BorderSide.none),
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 15.0, vertical: 7),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 10.h,
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: SizedBox(
                          height: double.infinity,
                          width: MediaQuery.of(context).size.width * 0.85,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: 3.h,
                                width: double.infinity,
                                alignment: Alignment.centerLeft,
                                child: text('Password',
                                    color: AppColors.Label,
                                    weight: FontWeight.w500,
                                    size: 10.sp),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.90,
                                height: 6.h,
                                decoration: const BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Material(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  elevation: 5,
                                  shadowColor: Colors.black,
                                  color: Colors.white,
                                  child: TextFormField(
                                    controller: passwordController,
                                    focusNode: passwordFocusNode,
                                    keyboardType: TextInputType.emailAddress,
                                    cursorColor: AppColors.buttoncolor,
                                    style: GoogleFonts.notoSans(
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w500,
                                      color: const Color.fromRGBO(34, 16, 0, 1),
                                    ),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Password is Empty';
                                      }
                                      if (value.length < 8) {
                                        return 'Please enter a valid Password';
                                      }
                                      return null;
                                    },
                                    obscureText: !passwordvisible,
                                    decoration: InputDecoration(
                                      hintText: 'Password',
                                      labelStyle: GoogleFonts.montserrat(
                                          fontSize: 9.sp,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              const Color.fromRGBO(0, 0, 0, 1)),
                                      suffixIcon: Container(
                                        width: 70,
                                        alignment: Alignment.center,
                                        child: text("FORGOT",
                                            color: AppColors.Label,
                                            weight: FontWeight.w500,
                                            size: 8.sp),
                                      ),
                                      prefixIcon: Container(
                                          height: 50,
                                          width: 55,
                                          alignment: Alignment.center,
                                          child: passwordvisible == false
                                              ? IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      passwordvisible = true;
                                                    });
                                                  },
                                                  icon: Lock)
                                              : IconButton(
                                                  icon: const Icon(
                                                    Icons.lock_open,
                                                    size: 18,
                                                  ),
                                                  onPressed: () {
                                                    setState(() {
                                                      passwordvisible = false;
                                                    });
                                                  },
                                                )),
                                      border: const OutlineInputBorder(
                                          borderSide: BorderSide.none),
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              horizontal: 15.0, vertical: 7),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.25,
                        height: 3.5.h,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: AppColors.buttoncolor,
                              shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                              ),
                              side: const BorderSide(width: 0)),
                          onPressed: () {
                            setState(() {
                              _formkey.currentState!.validate();
                              isLoading = true;
                              loginPost();
                            });
                            Future.delayed(const Duration(seconds: 3), () {
                              setState(() {
                                isLoading = false;
                                /*    Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const Dashboard()));*/
                              });
                            });
                          },
                          child: isLoading
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 2.h,
                                      width: 2.h,
                                      child: const CircularProgressIndicator(
                                        color: Colors.white,
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  ],
                                )
                              : Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    text("LOGIN",
                                        color: AppColors.buttontextColor,
                                        weight: FontWeight.w600,
                                        size: 8.sp),
                                    const SizedBox(
                                      width: 1,
                                    ),
                                    loginarrow
                                  ],
                                ),
                        ),
                      ),
                      SizedBox(
                        height: 4.h,
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            text('Don`t have an account?',
                                color: Colors.black,
                                weight: FontWeight.w500,
                                size: 10.sp),
                            TextButton(
                                onPressed: () {
                                  setState(() {
                                    Navigator.pushNamed(context, '/signup');
                                  });
                                  // Navigator.push(S
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             SignupScreen()));
                                  print('null');
                                },
                                child: text('sign up',
                                    color: AppColors.signuptext,
                                    weight: FontWeight.w500,
                                    size: 10.sp))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
