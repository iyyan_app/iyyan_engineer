import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../CommenScreen/Allassets.dart';
import '../CommenScreen/AppColors.dart';
import '../CommenScreen/CommenWidget.dart';

class DemoOption extends StatelessWidget {
  const DemoOption({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.h,
      alignment: Alignment.center,
      child: Container(
        height: 60.h,
        width: MediaQuery.of(context).size.width * 0.7,
        decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: AppColors.demoshadowcolor,
                offset: Offset(1, 1),
                blurRadius: 5,
                spreadRadius: 1,
              )
            ],
            color: AppColors.demoCardcolor,
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Column(
          children: [
            Container(
              height: 20.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 17.h,
                width: MediaQuery.of(context).size.width * 0.62,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.demoshadowcolor,
                        offset: Offset(1, 1),
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    text('Demo',
                        color: AppColors.chekboxActiveColor,
                        weight: FontWeight.w500,
                        size: 25.sp),
                    const SizedBox(
                      height: 10,
                    ),
                    text('Free trail for 15 days',
                        color: AppColors.commenTextColor,
                        weight: FontWeight.w500,
                        size: 10.sp)
                  ],
                ),
              ),
            ),
            Container(
              height: 40.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 37.h,
                width: MediaQuery.of(context).size.width * 0.62,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.demoshadowcolor,
                        offset: Offset(1, 1),
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    SizedBox(
                      height: 30.h,
                      width: double.infinity,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5.h,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Right,
                                const SizedBox(
                                  width: 10,
                                ),
                                text('Lorem Ipsum',
                                    color: AppColors.commenTextColor,
                                    weight: FontWeight.w500,
                                    size: 12.sp)
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 7.h,
                      alignment: Alignment.center,
                      child: Container(
                        height: 5.h,
                        width: MediaQuery.of(context).size.width * 0.5,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: AppColors.demoshadowcolor,
                                offset: Offset(1, 1),
                                blurRadius: 5,
                                spreadRadius: 1,
                              )
                            ],
                            color: AppColors.buttoncolor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: text('TRY NOW',
                            color: AppColors.buttontextColor,
                            weight: FontWeight.w500,
                            size: 12.sp),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Premium extends StatelessWidget {
  const Premium({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.h,
      alignment: Alignment.center,
      child: Container(
        height: 60.h,
        width: MediaQuery.of(context).size.width * 0.7,
        decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: AppColors.demoshadowcolor,
                offset: Offset(1, 1),
                blurRadius: 5,
                spreadRadius: 1,
              )
            ],
            color: AppColors.primiumCardcolor,
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Column(
          children: [
            Container(
              height: 20.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 17.h,
                width: MediaQuery.of(context).size.width * 0.62,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.demoshadowcolor,
                        offset: Offset(1, 1),
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    Container(
                      height: 5.h,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(left: 20),
                      child: text("Premium",
                          color: AppColors.chekboxActiveColor,
                          weight: FontWeight.w500,
                          size: 12.sp),
                    ),
                    Container(
                      height: 6.h,
                      alignment: Alignment.center,
                      child: text('\u20B9 5000',
                          color: AppColors.commenTextColor,
                          weight: FontWeight.w500,
                          size: 25.sp),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    text('6 Month',
                        color: AppColors.commenTextColor,
                        weight: FontWeight.w500,
                        size: 10.sp)
                  ],
                ),
              ),
            ),
            Container(
              height: 40.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 37.h,
                width: MediaQuery.of(context).size.width * 0.62,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.demoshadowcolor,
                        offset: Offset(1, 1),
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    SizedBox(
                      height: 30.h,
                      width: double.infinity,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5.h,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Right,
                                const SizedBox(
                                  width: 10,
                                ),
                                text('Lorem Ipsum',
                                    color: AppColors.commenTextColor,
                                    weight: FontWeight.w500,
                                    size: 12.sp)
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 7.h,
                      alignment: Alignment.center,
                      child: Container(
                        height: 5.h,
                        width: MediaQuery.of(context).size.width * 0.5,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: AppColors.demoshadowcolor,
                                offset: Offset(1, 1),
                                blurRadius: 5,
                                spreadRadius: 1,
                              )
                            ],
                            color: AppColors.buttoncolor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: text('BUY NOW',
                            color: AppColors.buttontextColor,
                            weight: FontWeight.w500,
                            size: 12.sp),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ProPremium extends StatelessWidget {
  const ProPremium({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.h,
      alignment: Alignment.center,
      child: Container(
        height: 60.h,
        width: MediaQuery.of(context).size.width * 0.7,
        decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: AppColors.demoshadowcolor,
                offset: Offset(1, 1),
                blurRadius: 5,
                spreadRadius: 1,
              )
            ],
            color: AppColors.buttoncolor,
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Column(
          children: [
            Container(
              height: 20.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 17.h,
                width: MediaQuery.of(context).size.width * 0.62,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.demoshadowcolor,
                        offset: Offset(1, 1),
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    Container(
                      height: 5.h,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(left: 20),
                      child: text("Pro Premium",
                          color: AppColors.chekboxActiveColor,
                          weight: FontWeight.w500,
                          size: 12.sp),
                    ),
                    Container(
                      height: 6.h,
                      alignment: Alignment.center,
                      child: text('\u20B9 10000',
                          color: AppColors.commenTextColor,
                          weight: FontWeight.w500,
                          size: 25.sp),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    text('one year',
                        color: AppColors.commenTextColor,
                        weight: FontWeight.w500,
                        size: 10.sp)
                  ],
                ),
              ),
            ),
            Container(
              height: 40.h,
              width: double.infinity,
              alignment: Alignment.center,
              child: Container(
                height: 37.h,
                width: MediaQuery.of(context).size.width * 0.62,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.demoshadowcolor,
                        offset: Offset(1, 1),
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    color: AppColors.background_Color,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    SizedBox(
                      height: 30.h,
                      width: double.infinity,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5.h,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Right,
                                const SizedBox(
                                  width: 10,
                                ),
                                text('Lorem Ipsum',
                                    color: AppColors.commenTextColor,
                                    weight: FontWeight.w500,
                                    size: 12.sp)
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 7.h,
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, '/ContinueScreen');
                        },
                        child: Container(
                          height: 5.h,
                          width: MediaQuery.of(context).size.width * 0.5,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: AppColors.demoshadowcolor,
                                  offset: Offset(1, 1),
                                  blurRadius: 5,
                                  spreadRadius: 1,
                                )
                              ],
                              color: AppColors.buttoncolor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: text('BUY NOW',
                              color: AppColors.buttontextColor,
                              weight: FontWeight.w500,
                              size: 12.sp),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MembershipScreen extends StatefulWidget {
  const MembershipScreen({super.key});

  @override
  State<MembershipScreen> createState() => _MembershipScreenState();
}

class _MembershipScreenState extends State<MembershipScreen> {
  List<dynamic> membership_Options = [
    {"id": 1, "name": 'Free trial'},
    {"id": 2, "name": 'Premium'},
    {"id": 3, "name": 'Pro Premium'},
  ];
  int _selectedPageIndex = 0;
  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  final List<Widget> _widgetOptions = <Widget>[
    // Billing(),
    const DemoOption(),
    const Premium(),
    const ProPremium()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: AppColors.background_Color,
          child: SingleChildScrollView(
              child: Column(children: [
            SizedBox(
              height: 16.h,
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    height: 16.h,
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: detailimg,
                  ),
                ],
              ),
            ),
            Container(
              height: 8.h,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  text('Select your',
                      color: AppColors.loginAndsigin,
                      weight: FontWeight.w600,
                      size: 15.sp),
                  text('Membership',
                      color: AppColors.loginAndsigin,
                      weight: FontWeight.w600,
                      size: 25.sp),
                ],
              ),
            ),
            SizedBox(
              height: 10.h,
              width: double.infinity,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: List.generate(
                      membership_Options.length,
                      (index) => InkWell(
                            onTap: () {
                              _selectPage(index);
                            },
                            child: Container(
                              height: 4.h,
                              width: MediaQuery.of(context).size.width * 0.3,
                              decoration: BoxDecoration(
                                  color: _selectedPageIndex == index
                                      ? AppColors.buttoncolor
                                      : AppColors.membershipinactiveColor,
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10))),
                              alignment: Alignment.center,
                              child: text(
                                  '${membership_Options[index]['name']}',
                                  color: _selectedPageIndex == index
                                      ? AppColors.background_Color
                                      : AppColors.commenTextColor,
                                  weight: FontWeight.w500,
                                  size: 11.sp),
                            ),
                          ))),
            ),
            SizedBox(
              height: 65.h,
              width: double.infinity,
              child: _widgetOptions.elementAt(_selectedPageIndex),
            )
          ]))),
    );
  }
}
